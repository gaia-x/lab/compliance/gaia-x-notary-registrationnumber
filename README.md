# Gaia-X registryNumber notarization API.

[[_TOC_]]


The registryNumber notarization API is a tool used to return a VC once all registrationNumber given by the participant
are successfully checked by the API.

## Process

The notarization process is the following

- Participant provides an object which must contains registrationNumber in the format given by the
  service-characteristics : https://gitlab.com/gaia-x/technical-committee/service-c/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/data-types/registration-number.yaml

- The API is doing the following checkups in this specific order:
    - Presence of the request body.
    - Presence of the registrationNumber formatted presentation.
    - Correctness of the provided types (gx:EORI / gx:vatID / gx:leiCode / schema:taxID). Incorrect types are ignored.
    - Correctness of the provided values :
        - If one external API at least is down, a specific error will be thrown.
        - If one value is not formatted accordingly, a specific error will be thrown. vatID, EORI and lei values
          should respect a specific format before even calling the external APIs. taxID values are varying among 
          the countries, so they will be passed to the external API as-is.

A signed VC of type `application/vc+jwt` will be returned, or an error  message.

## Existing deployments

In addition to the [GXDCH](https://gaia-x.eu/gxdch/) instances, the Gaia-X Lab maintains several instances:

| Deployment URL                                                                | Usage                           | Content                                                                                       |
|-------------------------------------------------------------------------------|---------------------------------|-----------------------------------------------------------------------------------------------|
| [`v1`, `v1.x.x`](https://registration.lab.gaia-x.eu/v1/docs/)                 |                                 | Tagus release. Version deployed on the Clearing Houses                                                                               |
| [`v1-staging`](https://registration.lab.gaia-x.eu/v1-staging/docs/)         |                                 | Tagus release without production rules                                                        |
| [`v2`, `v2.x.x`](https://registrationnumber.notary.lab.gaia-x.eu/v2/docs/)                 |                                 | Latest stable release.                                 |
| [`main`](https://registrationnumber.notary.lab.gaia-x.eu/main/docs/)                       | Used for playground activities. | Latest stable Loire (main branch)                                                                   |
| [`development`](https://registrationnumber.notary.lab.gaia-x.eu/development/docs/)         | Used for playground activities. | Latest unstable Loire (development branch)                                                          |


### Branch-off
Regarding end of Tagus implementation and branch-off for Loire release, we changed a bit the branch organization as follows:
- v1 branch host code for Loire release and is maintenance mode (fixes only)
- development host v2/Loire latest code
- main host v2/Loire latest stable code and triggers v2 image releases


## Images tags

This repo provides
several [images tags](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/container_registry/3872411).

| tag           | content              | example |
|---------------|----------------------|---------|
| `vX`          | latest major version | v1      |
| `vX.Y`        | latest minor version | v1.1    |
| `vX.Y.Z`      | specific version     | v1.1.1  |
| `main`        | latest stable        |         |
| `development` | latest unstable      |         |

## Routes

`/registrationNumberVC` will return a signed VC.

## Swagger

For compatibility reasons with other projects from the lab, documentation is also available under: `/docs` 

## Running locally

Please use a Node.js LTS version and install the required dependencies with the following commands.

```bash
nvm install
npm install
```

To set up the `.env` file, which the application needs to run, please execute the following command.

```bash
npm run setup-env
```

**Note:** this command runs the `env-setup.ts` script which creates a PKCS#8 private key and the corresponding x509 
certificate before inserting their values in the `.env` files.
{: .note}

You can then start the application.

```bash
npm run start:dev
```

## Running Tests

Tests are made with the Jest framework.
To run tests, run the following command.

```bash
  npm run test
```

Additional end-to-end tests are also available, they can be executed with the command below.

```bash
npm run test:e2e
```

## Deployment

A helm chart is provided inside /k8s/gx-notary folder.

It provides several environment variables for the application:

| Env Variable                       | Name in values file            | Default value                                                                 | Note                                                                                                                                                                                                         |
|------------------------------------|--------------------------------|-------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| APP_PATH                           | ingress.hosts[0].paths[0].path | /main                                                                         | Deployment path of the application. ⚠️ Please make sure to have the `/` character before the path ⚠️                                                                                                         |
| BASE_URL                           | baseUrl                        | http://localhost:3000/main                                                    | The base URL of the application, this contains the protocol, hostname, port and the above APP_PATH                                                                                                           |
| PRIVATE_KEY                        | PRIVATE_KEY                    | base64 value of "empty"                                                       | The signing private key in PKCS#8 PEM format. Stored in a secret in the cluster.  provided on commandline during deployment via --set PRIVATE_KEY=<privateKeyValue>                                          |
| PRIVATE_KEY_ALGORITHM              | PRIVATE_KEY_ALGORITHM          | PS256                                                                         | The algorithm for the provided private key, example(PS256)                                                                                                                                                   |
| X509_CERTIFICATE                   | X509_CERTIFICATE               | Use the value defined in "static/.well-known/x509CertificateChain.pem"        | Stored in a secret in the cluster                                                                                                                                                                            |
| EORI_VALIDATION_API                | eoriValidationAPI              | https://ec.europa.eu/taxation_customs/dds2/eos/validation/services/validation | European Commission API used to validate Economic Operators Registration and Identification (EORIs)                                                                                                          |
| LEI_CODE_VALIDATION_API            | leiCodeValidationApi           | https://api.gleif.org/api/v1/lei-records/                                     | Global Legal Entity Identifier Foundation API used to validate Legal Entity Identifiers (LEI Codes)                                                                                                          |
| VAT_ID_VALIDATION_API              | vatIdValidationAPI             | http://ec.europa.eu/taxation_customs/vies/services/checkVatService            | European Commission API used to validate Value-Added Tax identifiers (VAT IDs)                                                                                                                               |
| OPEN_CORPORATES_VALIDATION_API     | openCorporatesAPI              | https://api.opencorporates.com/v0.4/companies/search                          | Open Corporate API used to validate company numbers contained in [international company registers](https://opencorporates.com/registers)                                                                     |
| OPEN_CORPORATES_VALIDATION_API_KEY | openCorporatesAPIKey           |                                                                               | Optional. Requires a commercial license from [OpenCorporates Limited](https://opencorporates.com). If this value is not provided (or left blank), support for validation of `schema:taxID` type is disabled. |
| OFFLINE_CONTEXTS                   | offlineContexts                | false                                                                         | Whenever to use regular web document loader for context resolution instead of cached versions                                                                                                                |
| DEBUG_REQUESTS                     | debugRequests                  |                                                                               | Enables incoming request debug logs                                                                                                                                                                          |
| DEBUG_RESPONSES                    | debugResponses                 |                                                                               | Enables outgoing response debug logs                                                                                                                                                                         |

### Preparing Helm Values

Before running the Helm chart installation, the default values need to be tweaked to add the signing private key and related certificate.
Default values can be listed with the following command.

```shell
helm show values k8s/gx-notary 
```

We will focus on the `PRIVATE_KEY` and `X509_CERTIFICATE` values. **Those values need to be Base64 encoded strings**.  
For this example we will use the private key and certificate generated by the `npm run setup-env` in the [Running locally](## Running locally) section.

```bash
npm run setup-env # in case it wasn't executed before this (can be ignored otherwise)
source .env # load the variables in the current console session

encoded_private_key=$(echo $PRIVATE_KEY | base64) # encode the private key
encoded_certificate=$(echo $X509_CERTIFICATE | base64) # encode the certificate

truncate -s 0 custom-values.yml
echo "image:" >> custom-values.yml
echo "  tag: v2.0.0" >> custom-values.yml # v2.0.0 can be replaced by any version
echo "PRIVATE_KEY: $encoded_private_key" >> custom-values.yml
echo "X509_CERTIFICATE: $encoded_certificate" >> custom-values.yml
```

If other values need to be customized, they can be added to the `custom-values.yml` file.

> **WARNING**: If you are using an elliptic curve key, make sure that it's in PKCS#8 and not in SEC1/PEM.
If your key starts with `-----BEGIN EC PRIVATE KEY-----` please convert it using the following command:
```shell
openssl pkcs8 -topk8 -nocrypt -in ec1.pem -out ec2.pem
```

#### Development Image Deployment

In case you need to deploy your local development image, you can use the following commands.

```bash
npm run setup-env # in case it wasn't executed before this (can be ignored otherwise)
source .env # load the variables in the current console session

encoded_private_key=$(echo $PRIVATE_KEY | base64) # encode the private key
encoded_certificate=$(echo $X509_CERTIFICATE | base64) # encode the certificate

truncate -s 0 custom-values.yml
echo "image:" >> custom-values.yml
echo "  repository: gx-notary" >> custom-values.yml # this is mandatory if you want to deploy from your local Docker image registry
echo "  tag: dev" >> custom-values.yml # this is mandatory if you want to deploy from your local Docker image registry
echo "  pullPolicy: Never" >> custom-values.yml # this is mandatory if you want to deploy from your local Docker image registry
echo "PRIVATE_KEY: $encoded_private_key" >> custom-values.yml
echo "X509_CERTIFICATE: $encoded_certificate" >> custom-values.yml

docker build -t gx-notary:dev .
```

### Installing Helm Chart

```shell
helm upgrade --install --namespace "gaia-x" --create-namespace -f custom-values.yml gx-notary ./k8s/gx-notary
```

The `--namespace` value can be changed to your liking.  
The deployment is triggered automatically on `development` and `main` branches. Please refer to [server](#existing-deployments) for available instances.

### Checking the Gaia-X Notary

Once the Helm chart is correctly installed, you can either access the Gaia-X Notary through the ingress you specified or through port-forwarding as follows.

```bash
kubectl -n gaia-x port-forward services/gx-notary-main 3000:3000
```

You can check out the endpoints of your freshly deployed Gaia-X Notary at [http://localhost:3000/main/docs/](http://localhost:3000/main/docs/).

### Containers signature

Containers are signed using [cosign](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html). You can assert yourself
that the containers are signed using cosign client [verify](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html#container-images-1)

Example verifying the signature of the image built for tag `v1.7.0`:

```shell
docker run -it bitnami/cosign:latest verify --certificate-identity "https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber//.gitlab-ci.yml@refs/head/development" --certificate-oidc-issuer "https://gitlab.com" registry.gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber:development
```

#### Cluster policy using Kyverno

The k8s folder contains a Kyverno Policy ensuring the image you're deploying is properly signed and issued from Gaia-X AISBL

If you have deployed Kyverno on your cluster, this will be enforced automatically on each deployment.
