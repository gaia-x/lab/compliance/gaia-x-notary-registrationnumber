import * as fs from 'fs'
import { pki } from 'node-forge'

import { CertificateBuilder } from './src/common/test/certificate-builder'
import { KeyBuilder } from './src/utils/key-builder'

const keyPair: pki.KeyPair = KeyBuilder.buildKeyPair()
const certificate: pki.Certificate = CertificateBuilder.createCertificate(keyPair)

const x509Certificate = pki.certificateToPem(certificate)
const privateKey = KeyBuilder.convertToPKCS8(keyPair.privateKey)
const privateKeyAlgorithm = KeyBuilder.getPrivateKeyAlgorithm(certificate.siginfo.algorithmOid)

const envData = `APP_PATH=/main
BASE_URL=http://localhost:3000/main
OFFLINE_CONTEXTS=false

X509_CERTIFICATE='${x509Certificate.trim()}'
PRIVATE_KEY='${privateKey.trim()}'
PRIVATE_KEY_ALGORITHM='${privateKeyAlgorithm}'

LEI_CODE_VALIDATION_API='https://api.gleif.org/api/v1/lei-records/'
VAT_ID_VALIDATION_API='https://ec.europa.eu/taxation_customs/vies/services/checkVatService'
EORI_VALIDATION_API='https://ec.europa.eu/taxation_customs/dds2/eos/validation/services/validation'
OPEN_CORPORATES_VALIDATION_API='https://api.opencorporates.com/v0.4/companies/search'
OPEN_CORPORATES_VALIDATION_API_KEY=''`

fs.writeFileSync('.env', envData)
