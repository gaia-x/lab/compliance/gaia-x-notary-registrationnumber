FROM node:20-alpine AS builder

RUN apk upgrade --no-cache

USER node
WORKDIR /usr/src/app
COPY --chown=node package*.json ./
COPY --chown=node src/ src/
COPY --chown=node tsconfig.build.json tsconfig.build.json
COPY --chown=node tsconfig.json tsconfig.json
COPY --chown=node nest-cli.json nest-cli.json

RUN npm install && npm run build

FROM node:20-alpine

RUN apk upgrade --no-cache

USER node
WORKDIR /usr/app

COPY --from=builder --chown=node /usr/src/app/node_modules node_modules
COPY --from=builder --chown=node /usr/src/app/dist dist

CMD ["node", "dist/src/main"]
EXPOSE 3000

