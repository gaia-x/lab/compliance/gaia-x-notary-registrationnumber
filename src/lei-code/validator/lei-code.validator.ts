import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DateTime } from 'luxon'
import { Observable, catchError, map, of } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { RegistrationNumberValidator } from '../../common/validator/registration-number.validator'
import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'

@Injectable()
export class LeiCodeValidator implements RegistrationNumberValidator<LeiCodeValidationPayload> {
  private readonly logger = new Logger(LeiCodeValidator.name)
  private readonly apiUrl: string

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
  ) {
    this.apiUrl = this.configService.get<string>('LEI_CODE_VALIDATION_API')?.trim()

    if (!this.apiUrl) {
      throw new Error('Please provide the GLEIF API URL through LEI_CODE_VALIDATION_API')
    }

    this.logger.debug(`Initializing LEI code validator with (API: ${this.apiUrl})`)
  }

  validate(leiCode: string): Observable<LeiCodeValidationPayload> {
    this.logger.log(`Calling GLEIF API to validate LEI code ${leiCode}`)

    const url = `${this.apiUrl}${leiCode}`

    return this.httpService
      .get(url, {
        headers: {
          Accept: 'application/vnd.api+json'
        }
      })
      .pipe(
        map(response => {
          if (response.status === 200) {
            return new LeiCodeValidationPayload(
              response.data.data.attributes.lei,
              true,
              {
                'iso3166-1': response.data.data.attributes.entity.headquartersAddress.country,
                'iso3166-2': response.data.data.attributes.entity.headquartersAddress.region
              },
              new ValidationEvidence(url, DateTime.now(), RegistrationNumberTypeEnum.LEI_CODE)
            )
          }
        }),
        catchError(error => {
          this.logger.warn(`Failed to validate LEI code with value ${leiCode} : ${error}`)

          return of(
            new LeiCodeValidationPayload(leiCode, false, null, new ValidationEvidence(url, DateTime.now(), RegistrationNumberTypeEnum.LEI_CODE))
          )
        })
      )
  }
}
