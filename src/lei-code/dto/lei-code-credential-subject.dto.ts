import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from '../../common/dto/credential-subject.dto'

export class LeiCodeCredentialSubjectDto extends CredentialSubjectDto {
  @ApiProperty()
  'schema:leiCode': string

  @ApiProperty()
  'gx:countryCode': string

  @ApiProperty()
  'gx:subdivisionCountryCode': string
}
