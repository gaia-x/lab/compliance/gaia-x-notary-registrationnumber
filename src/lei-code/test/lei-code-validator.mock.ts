import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'

export const VALID_LEI_CODE: string = '9695007586GCAKPYJ703'
export const INVALID_LEI_CODE: string = '9695007586GCAKPYJ123'

export const LeiCodeValidatorMock = {
  validate: jest.fn((leiCode: string): Observable<LeiCodeValidationPayload> => {
    return new Observable(subscriber => {
      const valid = leiCode === VALID_LEI_CODE

      subscriber.next(
        new LeiCodeValidationPayload(
          leiCode,
          valid,
          valid
            ? {
                'iso3166-1': 'FR',
                'iso3166-2': 'FR-NAQ'
              }
            : null,
          new ValidationEvidence('https://validator.eu/checkLeiCodeService', DateTime.now(), RegistrationNumberTypeEnum.LEI_CODE),
          'LeiCode validation error message'
        )
      )
      subscriber.complete()
    })
  })
}
