import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { ValidationPayload } from '../../common/validator/model/validation-payload'

interface CountryCodes {
  'iso3166-1': string
  'iso3166-2': string
}

export class LeiCodeValidationPayload extends ValidationPayload {
  constructor(
    value: string,
    valid: boolean,
    private readonly _countryCodes: CountryCodes,
    evidence: ValidationEvidence,
    invalidationReason?: string
  ) {
    super(RegistrationNumberTypeEnum.LEI_CODE, value, valid, evidence, invalidationReason)
  }

  get countryCodes(): CountryCodes {
    return this._countryCodes
  }
}
