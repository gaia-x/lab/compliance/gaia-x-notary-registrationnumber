import { PreAuthorizedCodeUtils } from '@gaia-x/oidc4vc'
import { Controller, Get, Header, HttpCode, HttpException, HttpStatus, Inject, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiExcludeEndpoint,
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProduces,
  ApiQuery,
  ApiTags
} from '@nestjs/swagger'
import { lastValueFrom } from 'rxjs'

import { DocumentDto, VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { CertificateExpiryGuard } from '../../common/guard/certificate-expiry.guard'
import { RegistrationNumberInputGuard } from '../../common/guard/registration-number-input.guard'
import { DocumentMapper } from '../../common/mapper/document.mapper'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { OIDC4VCIService } from '../../oidc4vci/oidc4vci.service'
import { LeiCodeCredentialSubjectDto } from '../dto/lei-code-credential-subject.dto'
import { LeiCodeDocumentMapper } from '../mapper/lei-code-document.mapper'
import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'
import { LeiCodeValidator } from '../validator/lei-code.validator'

@ApiTags('registration-number')
@Controller()
@UseGuards(CertificateExpiryGuard, RegistrationNumberInputGuard)
export class LeiCodeController {
  private readonly documentMapper: DocumentMapper<LeiCodeValidationPayload, LeiCodeCredentialSubjectDto>

  constructor(
    @Inject('DidWeb') private readonly didWeb: string,
    @Inject('OntologyVersion') private readonly ontologyVersion: string,
    private readonly leiCodeValidator: LeiCodeValidator,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly oidc4vciService: OIDC4VCIService
  ) {
    this.documentMapper = new LeiCodeDocumentMapper(this.didWeb, ontologyVersion)
  }

  @Get('registration-numbers/lei-code/:leiCode')
  @ApiOperation({
    summary: `Produce a registration number verifiable credential from a Legal Entity Identifier (LEI) code`,
    description: `This endpoint checks the given Legal Entity Identifier (LEI) code against the Global Legal Entity 
    Identifier Foundation (GLEIF) API and responds with a verifiable credential attesting the LEI code's validity as 
    a registration number`
  })
  @HttpCode(200)
  @Header('Content-Type', 'application/vc+jwt')
  @ApiProduces('Content-Type', 'application/vc+jwt')
  @ApiQuery({
    name: 'vcId',
    description: `The output verifiable credential ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/credentials/123'
  })
  @ApiQuery({
    name: 'subjectId',
    description: `The output credential subject ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/subjects/123'
  })
  @ApiParam({
    name: 'leiCode',
    description: `The Legal Entity Identifier (LEI) code to verify and produce the verifiable credential from`,
    type: 'string',
    required: true,
    example: '9695007586GCAKPYJ703'
  })
  @ApiExtraModels(VerifiableCredentialDto)
  @ApiOkResponse({
    description: `Certified registration number verifiable credential`,
    content: {
      'application/vc+jwt': {
        schema: {
          type: 'string',
          description: 'JSON Web Token'
        },
        example:
          'eyJhbGciOiJQUzI1NiIsImlzcyI6ImRpZDp3ZWI6bG9jYWxob3N0OjMwMDAiLCJraWQiOiJkaWQ6d2ViOmxvY2FsaG9zdDozMDAwI1g1MDktSldLIiwiaWF0IjoxNzIxMDUyMjY0MjIyLCJleHAiOjE3Mjg4MjgyNjQyMjIsImN0eSI6InZjK2xkIiwidHlwIjoidmMrbGQrand0In0.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9kZXZlbG9wbWVudCMiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvY3JlZGVudGlhbHMvMTIzIiwibmFtZSI6IkxFSSBDb2RlIiwiZGVzY3JpcHRpb24iOiJMZWdhbCBFbnRpdHkgSWRlbnRpZmllciIsImlzc3VlciI6ImRpZDp3ZWI6bG9jYWxob3N0OjMwMDAiLCJ2YWxpZEZyb20iOiIyMDI0LTA3LTE1VDE2OjA0OjI0LjIyMiswMjowMCIsInZhbGlkVW50aWwiOiIyMDI0LTEwLTEzVDE2OjA0OjI0LjIyMiswMjowMCIsImNyZWRlbnRpYWxTdWJqZWN0Ijp7IkBjb250ZXh0Ijp7InNjaGVtYSI6Imh0dHBzOi8vc2NoZW1hLm9yZy8ifSwiaWQiOiJodHRwczovL2V4YW1wbGUub3JnL3N1YmplY3RzLzEyMyIsInR5cGUiOiJneDpMZWlDb2RlIiwic2NoZW1hOmxlaUNvZGUiOiI5Njk1MDA3NTg2R0NBS1BZSjcwMyIsImd4OmNvdW50cnlDb2RlIjoiRlIiLCJneDpzdWJkaXZpc2lvbkNvdW50cnlDb2RlIjpudWxsfSwiZXZpZGVuY2UiOnsiZ3g6ZXZpZGVuY2VPZiI6IkxFSV9DT0RFIiwiZ3g6ZXZpZGVuY2VVUkwiOiJodHRwczovL2FwaS5nbGVpZi5vcmcvYXBpL3YxL2xlaS1yZWNvcmRzLzk2OTUwMDc1ODZHQ0FLUFlKNzAzIiwiZ3g6ZXhlY3V0aW9uRGF0ZSI6IjIwMjQtMDctMTVUMTY6MDQ6MjQuMjIxKzAyOjAwIn19.Dj8S4ONFEp3-12JBIfIP_Ru_B6xtky-9W1UDOWSlytNGHlQKkPZZmMUP9Ad9TPyvfEaG86kYWltlEZEscIjtYyTgLJPQqZkzqUZwdKsmu78kgXCV8QU30CVrQ2npwdJEtfC94H-fq6zcMZW9MOEBd9jUy-nHORbzu0krNWP_AxMJq_PQZeCEr0ur7D2bFSz2kWtz3RawDfp1zt0UpAuDHoToAUJ4c9UgcW-W5ZemVFT9PAycLUl0IvuaTxOja-C0LIuw1Ma3W5PVZfXsjNrCdvVfFsOi_No6ic_u3DrSdc0-zdg7tBwUCcENHKjG9wAgPJ7gajq-t-a9OfVtyrpshw'
      }
    }
  })
  @ApiBadRequestResponse({
    description: `Invalid legal registration number verifiable credential`,
    content: {
      'application/json': {
        examples: {
          'Payload missing': {
            value: {
              statusCode: 400,
              message: 'The request payload is missing'
            }
          },
          'Verifiable credential ID missing': {
            value: {
              statusCode: 400,
              message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
            }
          },
          'Subject ID missing': {
            value: {
              statusCode: 400,
              message: 'The subject ID is missing, please provide it through the subjectId query parameter'
            }
          },
          'Invalid registration number or API error': {
            value: {
              statusCode: 400,
              message: `The given leiCode: [9695007586GCAKPYJ703] couldn't be validated: error message`
            }
          }
        }
      }
    }
  })
  async checkLeiCode(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('leiCode') leiCode: string): Promise<string> {
    const validationPayload: LeiCodeValidationPayload = await lastValueFrom(this.leiCodeValidator.validate(leiCode))

    if (!validationPayload.valid) {
      throw new HttpException(
        `The given leiCode: [${leiCode}] couldn't be validated: ${validationPayload.invalidationReason}`,
        HttpStatus.BAD_REQUEST
      )
    }

    const document: DocumentDto = this.documentMapper.mapFromValidationPayload(vcId, subjectId, validationPayload)

    return this.jwtSignatureService.signVerifiableCredential(document)
  }

  @ApiExcludeEndpoint()
  @Get('registration-numbers/leiCode/:leiCode/credential')
  async issueLeiCode(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('leiCode') leiCode: string) {
    const signedDocument = await this.checkLeiCode(vcId, subjectId, leiCode)
    // @ts-expect-error Will be fixed when @gaia-x/oidc4vc support https://www.w3.org/TR/vc-data-model-2.0/
    return await this.oidc4vciService.createCredentialOffer(PreAuthorizedCodeUtils.generate(), signedDocument)
  }
}
