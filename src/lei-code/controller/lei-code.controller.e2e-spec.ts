import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import { JWTVerifyResult, importSPKI, jwtVerify } from 'jose'
import * as jsonld from 'jsonld'
import { DateTime } from 'luxon'
import * as request from 'supertest'

import { PRIVATE_KEY_ALGORITHM, PUBLIC_KEY } from '../../common/constants'
import { VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { LeiCodeModule } from '../lei-code.module'
import { INVALID_LEI_CODE, LeiCodeValidatorMock, VALID_LEI_CODE } from '../test/lei-code-validator.mock'
import { LeiCodeValidator } from '../validator/lei-code.validator'

jest.mock('jsonld')

describe('LeiCodeController', () => {
  let app: NestApplication
  const mockConfigService: ConfigServiceMock = new ConfigServiceMock({
    BASE_URL: 'http://127.0.0.1:3000/main',
    OFFLINE_CONTEXTS: 'false'
  }).withKeyPairAndCertificate()

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [LeiCodeModule]
    })
      .overrideProvider(LeiCodeValidator)
      .useValue(LeiCodeValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app?.close()
  })

  describe('/registration-numbers/lei-code/:leiCode', () => {
    it('should validate and sign a verifiable credential', async () => {
      ;(jsonld.canonize as jest.Mock).mockResolvedValue('normalizedVerifiableCredential')

      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/lei-code/${VALID_LEI_CODE}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(200)
        .expect('Content-Type', 'application/vc+jwt; charset=utf-8')

      const publicKey = await importSPKI(mockConfigService.get(PUBLIC_KEY), mockConfigService.get(PRIVATE_KEY_ALGORITHM))
      const results: JWTVerifyResult<VerifiableCredentialDto> = await jwtVerify(response.text, publicKey)
      expect(results).toBeDefined()
      expect(results.payload).toBeDefined()
      expect(results.payload.id).toBeDefined()

      expect(results.payload['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
      expect(results.payload.id).toEqual('https://example.org/credentials/123')
      expect(results.payload.name).toEqual('LEI Code')
      expect(results.payload.description).toEqual('Legal Entity Identifier')
      expect(results.payload.issuer).toEqual('did:web:127.0.0.1:3000:main')
      expect(DateTime.fromISO(results.payload.validFrom).diffNow().milliseconds).toBeLessThan(5000)
      expect(results.payload.validUntil).not.toBeNull()
      // Credential subject assertions
      expect(results.payload.credentialSubject['@context']).toEqual({ schema: 'https://schema.org/' })
      expect(results.payload.credentialSubject.id).toEqual('https://example.org/subjects/123')
      expect(results.payload.credentialSubject.type).toEqual('gx:LeiCode')
      expect(results.payload.credentialSubject['schema:leiCode']).toEqual(VALID_LEI_CODE)
      expect(results.payload.credentialSubject['gx:countryCode']).toEqual('FR')
      expect(results.payload.credentialSubject['gx:subdivisionCountryCode']).toEqual('FR-NAQ')
      // Evidence assertions
      expect(results.payload.evidence['gx:evidenceOf']).toEqual('gx:LeiCode')
      expect(results.payload.evidence['gx:evidenceURL']).toEqual('https://validator.eu/checkLeiCodeService')
    })

    it('should return a bad request if the verifiable credential ID is missing', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/lei-code/${VALID_LEI_CODE}`)
        .query({
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
        })
    })

    it('should return a bad request if the subject ID is missing', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/lei-code/${VALID_LEI_CODE}`)
        .query({
          vcId: 'https://example.org/credentials/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: 'The subject ID is missing, please provide it through the subjectId query parameter'
        })
    })

    it('should return a bad request if the LEI code is invalid', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/lei-code/${INVALID_LEI_CODE}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: `The given leiCode: [${INVALID_LEI_CODE}] couldn't be validated: LeiCode validation error message`
        })
    })
  })
})

describe('LeiCodeController with expired certificate', () => {
  let app: NestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [LeiCodeModule]
    })
      .overrideProvider(LeiCodeValidator)
      .useValue(LeiCodeValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'http://127.0.0.1:3000/main',
          OFFLINE_CONTEXTS: 'false'
        }).withPrivateKeyAndExpiredCertificate()
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  describe('/registration-numbers/lei-code/:leiCode', () => {
    it('should throw an expired certificate error', async () => {
      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/lei-code/${VALID_LEI_CODE}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(500)
        .expect('Content-Type', 'application/json; charset=utf-8')

      expect(response.body).toEqual({
        message: 'This instance certificate has expired and it will not be able to emit credentials',
        error: 'Internal Server Error',
        statusCode: 500
      })
    })
  })
})
