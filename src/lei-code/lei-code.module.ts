import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { JwtSignatureService } from '../jwt/service/jwt-signature.service'
import { OIDC4VCIModule } from '../oidc4vci/oidc4vci.module'
import { LeiCodeController } from './controller/lei-code.controller'
import { LeiCodeValidator } from './validator/lei-code.validator'

@Module({
  imports: [CommonModule, ConfigModule, HttpModule, OIDC4VCIModule],
  controllers: [LeiCodeController],
  providers: [LeiCodeValidator, JwtSignatureService]
})
export class LeiCodeModule {}
