import { CredentialSubjectMapper } from '../../common/mapper/credential-subject.mapper'
import { LeiCodeCredentialSubjectDto } from '../dto/lei-code-credential-subject.dto'
import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'

export class LeiCodeCredentialSubjectMapper implements CredentialSubjectMapper<LeiCodeValidationPayload, LeiCodeCredentialSubjectDto> {
  map(payload: LeiCodeValidationPayload, subjectId: string): LeiCodeCredentialSubjectDto {
    return {
      '@context': {
        schema: 'https://schema.org/'
      },
      id: subjectId,
      type: 'gx:LeiCode',
      'schema:leiCode': payload.value,
      'gx:countryCode': payload.countryCodes['iso3166-1'],
      'gx:subdivisionCountryCode': payload.countryCodes['iso3166-2']
    }
  }
}
