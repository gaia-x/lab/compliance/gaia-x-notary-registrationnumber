import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'
import { VALID_LEI_CODE } from '../test/lei-code-validator.mock'
import { LeiCodeCredentialSubjectMapper } from './lei-code-credential-subject.mapper'

describe('LeiCodeCredentialSubjectMapper', () => {
  it('should map a LEI code validation payload to a credential subject', () => {
    const mapper: LeiCodeCredentialSubjectMapper = new LeiCodeCredentialSubjectMapper()
    const validationPayload: LeiCodeValidationPayload = new LeiCodeValidationPayload(
      VALID_LEI_CODE,
      true,
      {
        'iso3166-1': 'FR',
        'iso3166-2': 'FR-NAQ'
      },
      null
    )

    expect(mapper.map(validationPayload, 'https://example.org/subjects/123')).toEqual({
      '@context': {
        schema: 'https://schema.org/'
      },
      id: 'https://example.org/subjects/123',
      type: 'gx:LeiCode',
      'schema:leiCode': VALID_LEI_CODE,
      'gx:countryCode': 'FR',
      'gx:subdivisionCountryCode': 'FR-NAQ'
    })
  })
})
