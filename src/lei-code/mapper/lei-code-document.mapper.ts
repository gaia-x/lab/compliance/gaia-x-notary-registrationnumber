import { DocumentMapper } from '../../common/mapper/document.mapper'
import { LeiCodeCredentialSubjectDto } from '../dto/lei-code-credential-subject.dto'
import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'
import { LeiCodeCredentialSubjectMapper } from './lei-code-credential-subject.mapper'

export class LeiCodeDocumentMapper extends DocumentMapper<LeiCodeValidationPayload, LeiCodeCredentialSubjectDto> {
  constructor(didWeb: string, ontologyVersion: string) {
    super(didWeb, ontologyVersion, new LeiCodeCredentialSubjectMapper(), 'LEI Code', 'Legal Entity Identifier')
  }
}
