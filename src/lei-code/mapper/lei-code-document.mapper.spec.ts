import { DateTime } from 'luxon'

import { DocumentDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { LeiCodeValidationPayload } from '../model/lei-code-validation-payload'
import { VALID_LEI_CODE } from '../test/lei-code-validator.mock'
import { LeiCodeDocumentMapper } from './lei-code-document.mapper'

describe('LeiCodeDocumentMapper', () => {
  it('should map a LEI code validation payload to a document', () => {
    const mapper: LeiCodeDocumentMapper = new LeiCodeDocumentMapper('did:web:gaia-x.eu', 'development')

    const document: DocumentDto = mapper.mapFromValidationPayload(
      'https://example.org/credentials/123',
      'https://example.org/subjects/123',
      new LeiCodeValidationPayload(
        VALID_LEI_CODE,
        true,
        {
          'iso3166-1': 'FR',
          'iso3166-2': 'FR-NAQ'
        },
        new ValidationEvidence('https://lei-code.org/check/', DateTime.now(), RegistrationNumberTypeEnum.LEI_CODE)
      )
    )

    expect(document['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
    expect(document.name).toEqual('LEI Code')
    expect(document.description).toEqual('Legal Entity Identifier')
  })
})
