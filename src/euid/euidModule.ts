import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { JwtSignatureService } from '../jwt/service/jwt-signature.service'
import { OIDC4VCIModule } from '../oidc4vci/oidc4vci.module'
import { EuidController } from './controller/euid.controller'
import { EuidValidator } from './validator/euid.validator'

@Module({
  imports: [CommonModule, ConfigModule, HttpModule, OIDC4VCIModule],
  controllers: [EuidController],
  providers: [EuidValidator, JwtSignatureService]
})
export class EuidModule {}
