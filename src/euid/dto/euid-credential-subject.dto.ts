import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from '../../common/dto/credential-subject.dto'

export class EuidCredentialSubjectDto extends CredentialSubjectDto {
  @ApiProperty()
  'gx:euid': string
}
