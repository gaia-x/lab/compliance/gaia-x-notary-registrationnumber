import { Injectable } from '@nestjs/common'
import { Observable } from 'rxjs'

import { RegistrationNumberValidator } from '../../common/validator/registration-number.validator'
import { EuidValidationPayload } from '../model/euid-validation-payload'

@Injectable()
export class EuidValidator implements RegistrationNumberValidator<EuidValidationPayload> {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  validate(euid: string): Observable<EuidValidationPayload> {
    // TODO: implement EUID check
    // Is there an unified API? Otherwise we may have to implement it per country.
    // https://ec.europa.eu/transparency/documents-register/api/files/SWD(2023)79?ersIds=de00000001046891 (2.3)
    throw new Error('EUID is not supported yet')
  }
}
