import { DateTime } from 'luxon'

import { DocumentDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { EuidValidationPayload } from '../model/euid-validation-payload'
import { VALID_EUID } from '../test/euid-validator.mock'
import { EuidDocumentMapper } from './euid-document.mapper'

describe('EuidDocumentMapper', () => {
  it('should map an EUID validation payload to a document', () => {
    const mapper: EuidDocumentMapper = new EuidDocumentMapper('did:web:gaia-x.eu', 'development')

    const document: DocumentDto = mapper.mapFromValidationPayload(
      'https://example.org/credentials/123',
      'https://example.org/subjects/123',
      new EuidValidationPayload(VALID_EUID, true, new ValidationEvidence('https://euid.org/check/', DateTime.now(), RegistrationNumberTypeEnum.EUID))
    )

    expect(document['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
    expect(document.name).toEqual('EUID')
    expect(document.description).toEqual('European Unique Identifier')
  })
})
