import { EuidValidationPayload } from '../model/euid-validation-payload'
import { VALID_EUID } from '../test/euid-validator.mock'
import { EuidCredentialSubjectMapper } from './euid-credential-subject.mapper'

describe('EuidCredentialSubjectMapper', () => {
  it('should map an EUID validation payload to a credential subject', () => {
    const mapper: EuidCredentialSubjectMapper = new EuidCredentialSubjectMapper()
    const validationPayload: EuidValidationPayload = new EuidValidationPayload(VALID_EUID, true, null)

    expect(mapper.map(validationPayload, 'https://example.org/subjects/123')).toEqual({
      id: 'https://example.org/subjects/123',
      type: 'gx:EUID',
      'gx:euid': VALID_EUID
    })
  })
})
