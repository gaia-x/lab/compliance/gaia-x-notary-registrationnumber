import { CredentialSubjectMapper } from '../../common/mapper/credential-subject.mapper'
import { EuidCredentialSubjectDto } from '../dto/euid-credential-subject.dto'
import { EuidValidationPayload } from '../model/euid-validation-payload'

export class EuidCredentialSubjectMapper implements CredentialSubjectMapper<EuidValidationPayload, EuidCredentialSubjectDto> {
  map(payload: EuidValidationPayload, subjectId: string): EuidCredentialSubjectDto {
    return {
      id: subjectId,
      type: 'gx:EUID',
      'gx:euid': payload.value
    }
  }
}
