import { DocumentMapper } from '../../common/mapper/document.mapper'
import { EuidCredentialSubjectDto } from '../dto/euid-credential-subject.dto'
import { EuidValidationPayload } from '../model/euid-validation-payload'
import { EuidCredentialSubjectMapper } from './euid-credential-subject.mapper'

export class EuidDocumentMapper extends DocumentMapper<EuidValidationPayload, EuidCredentialSubjectDto> {
  constructor(didWeb: string, ontologyVersion: string) {
    super(didWeb, ontologyVersion, new EuidCredentialSubjectMapper(), 'EUID', 'European Unique Identifier')
  }
}
