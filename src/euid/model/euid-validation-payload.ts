import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { ValidationPayload } from '../../common/validator/model/validation-payload'

export class EuidValidationPayload extends ValidationPayload {
  constructor(value: string, valid: boolean, evidence: ValidationEvidence) {
    super(RegistrationNumberTypeEnum.EUID, value, valid, evidence)
  }
}
