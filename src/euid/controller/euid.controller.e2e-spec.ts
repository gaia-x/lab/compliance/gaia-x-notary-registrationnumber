import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import * as request from 'supertest'

import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { EuidModule } from '../euidModule'
import { EuidValidatorMock, INVALID_EUID, VALID_EUID } from '../test/euid-validator.mock'
import { EuidValidator } from '../validator/euid.validator'

jest.mock('jsonld')

describe('EuidController', () => {
  let app: NestApplication
  const mockConfigService: ConfigServiceMock = new ConfigServiceMock({
    BASE_URL: 'http://127.0.0.1:3000/main',
    OFFLINE_CONTEXTS: 'false'
  }).withKeyPairAndCertificate()

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [EuidModule]
    })
      .overrideProvider(EuidValidator)
      .useValue(EuidValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  describe('/registration-numbers/euid/:euid', () => {
    it('should return a bad request as EUID is not supported to date', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/euid/${INVALID_EUID}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: `The given EUID: [${INVALID_EUID}] couldn't be validated: EUID validation error message`
        })
    })
  })
})

describe('EuidController with expired certificate', () => {
  let app: NestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [EuidModule]
    })
      .overrideProvider(EuidValidator)
      .useValue(EuidValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'http://127.0.0.1:3000/main',
          OFFLINE_CONTEXTS: 'false'
        }).withPrivateKeyAndExpiredCertificate()
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app?.close()
  })

  describe('/registration-numbers/euid/:euid', () => {
    it('should throw an expired certificate error', async () => {
      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/euid/${VALID_EUID}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/lei-codes/123'
        })
        .send()
        .expect(500)
        .expect('Content-Type', 'application/json; charset=utf-8')

      expect(response.body).toEqual({
        message: 'This instance certificate has expired and it will not be able to emit credentials',
        error: 'Internal Server Error',
        statusCode: 500
      })
    })
  })
})
