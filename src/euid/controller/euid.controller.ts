import { PreAuthorizedCodeUtils } from '@gaia-x/oidc4vc'
import { Controller, Get, Header, HttpCode, HttpException, HttpStatus, Inject, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiExcludeEndpoint,
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProduces,
  ApiQuery,
  ApiTags
} from '@nestjs/swagger'
import { lastValueFrom } from 'rxjs'

import { DocumentDto, VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { CertificateExpiryGuard } from '../../common/guard/certificate-expiry.guard'
import { RegistrationNumberInputGuard } from '../../common/guard/registration-number-input.guard'
import { DocumentMapper } from '../../common/mapper/document.mapper'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { OIDC4VCIService } from '../../oidc4vci/oidc4vci.service'
import { EuidCredentialSubjectDto } from '../dto/euid-credential-subject.dto'
import { EuidDocumentMapper } from '../mapper/euid-document.mapper'
import { EuidValidationPayload } from '../model/euid-validation-payload'
import { EuidValidator } from '../validator/euid.validator'

@ApiTags('registration-number')
@Controller()
@UseGuards(CertificateExpiryGuard, RegistrationNumberInputGuard)
export class EuidController {
  private readonly documentMapper: DocumentMapper<EuidValidationPayload, EuidCredentialSubjectDto>

  constructor(
    @Inject('DidWeb') private readonly didWeb: string,
    @Inject('OntologyVersion') private readonly ontologyVersion: string,
    private readonly euidValidator: EuidValidator,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly oidc4vciService: OIDC4VCIService
  ) {
    this.documentMapper = new EuidDocumentMapper(this.didWeb, ontologyVersion)
  }

  @Get('registration-numbers/euid/:euid')
  @ApiOperation({
    summary: `Produce a registration number verifiable credential from a European Unique Identifier (EUID)`,
    description: `This endpoint checks the given European Unique Identifier (EUID) against the 
    European Unique Identifier API and responds with a verifiable credential attesting the EUID's validity as a 
    registration number`
  })
  @HttpCode(200)
  @Header('Content-Type', 'application/vc+jwt')
  @ApiProduces('Content-Type', 'application/vc+jwt')
  @ApiQuery({
    name: 'vcId',
    description: `The output verifiable credential ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/credentials/123'
  })
  @ApiQuery({
    name: 'subjectId',
    description: `The output credential subject ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/lei-codes/123'
  })
  @ApiParam({
    name: 'euid',
    description: `The European Unique Identifier (EUID) to verify and produce the verifiable 
    credential from`,
    type: 'string',
    required: true,
    example: 'DE50670B34567760'
  })
  @ApiExtraModels(VerifiableCredentialDto)
  @ApiOkResponse({
    description: `Certified registration number verifiable credential`,
    content: {
      'application/vc+jwt': {
        schema: {
          type: 'string',
          description: 'JSON Web Token'
        },
        example:
          'eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9kZXZlbG9wbWVudCMiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvY3JlZGVudGlhbHMvMTIzIiwibmFtZSI6IkVVSUQgUmVnaXN0cmF0aW9uIE51bWJlciIsImRlc2NyaXB0aW9uIjoiRVVJRCByZWdpc3RyYXRpb24gbnVtYmVyIiwiaXNzdWVyIjoiZGlkOndlYjpnYWlhLXgudW5pY29ybi1ob21lLmNvbSIsInZhbGlkRnJvbSI6IjIwMjQtMDctMTVUMTY6MDc6MTkuMzIwKzAyOjAwIiwidmFsaWRVbnRpbCI6IjIwMjQtMDctMTZUMTQ6MTU6NDkuOTA1KzAwOjAwIiwiY3JlZGVudGlhbFN1YmplY3QiOnsiaWQiOiJodHRwczovL2V4YW1wbGUub3JnL3N1YmplY3RzLzEyMyIsInR5cGUiOiJneDpFVUlEIiwiZ3g6ZXVpZCI6IkRFNTA2NzBCMzQ1Njc3NjAiLCJneDpjb3VudHJ5IjoiIn0sImV2aWRlbmNlIjp7Imd4OmV2aWRlbmNlT2YiOiJFVUlEIiwiZ3g6ZXZpZGVuY2VVUkwiOiJOL0EiLCJneDpleGVjdXRpb25EYXRlIjoiMjAyNC0wNy0xNVQxNjowNzoxOS4zMjArMDI6MDAifX0.KONTd9RciMmBNRAvouaxrj-b7_AzneoQo-HzEbSHiFKhlt0FeNMYlwMFzlJbc3dstCHDdDjurmZspua3v7GCpw'
      }
    }
  })
  @ApiBadRequestResponse({
    description: `Invalid legal registration number verifiable credential`,
    content: {
      'application/json': {
        examples: {
          'Payload missing': {
            value: {
              statusCode: 400,
              message: 'The request payload is missing'
            }
          },
          'Verifiable credential ID missing': {
            value: {
              statusCode: 400,
              message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
            }
          },
          'Subject ID missing': {
            value: {
              statusCode: 400,
              message: 'The subject ID is missing, please provide it through the subjectId query parameter'
            }
          },
          'Invalid registration number or API error': {
            value: {
              statusCode: 400,
              message: `The given EUID: [DE50670B34567760] couldn't be validated: error message`
            }
          }
        }
      }
    }
  })
  async checkEuid(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('euid') euid: string): Promise<string> {
    const validationPayload: EuidValidationPayload = await lastValueFrom(this.euidValidator.validate(euid))

    if (!validationPayload.valid) {
      throw new HttpException(
        `The given EUID: [${euid}] couldn't be validated: ${validationPayload.invalidationReason}`,
        HttpStatus.BAD_REQUEST
      )
    }

    const document: DocumentDto = this.documentMapper.mapFromValidationPayload(vcId, subjectId, validationPayload)

    return this.jwtSignatureService.signVerifiableCredential(document)
  }

  @ApiExcludeEndpoint()
  @Get('registration-numbers/euid/:euid/credential')
  async issueEuid(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('euid') euid: string) {
    const signedDocument = await this.checkEuid(vcId, subjectId, euid)
    // @ts-expect-error Will be fixed when @gaia-x/oidc4vc support https://www.w3.org/TR/vc-data-model-2.0/
    return await this.oidc4vciService.createCredentialOffer(PreAuthorizedCodeUtils.generate(), signedDocument)
  }
}
