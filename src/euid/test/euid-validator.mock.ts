import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { ValidationPayload } from '../../common/validator/model/validation-payload'

export const VALID_EUID: string = 'DE50670B34567760'
export const INVALID_EUID: string = 'DE12345678910123'

export const EuidValidatorMock = {
  validate: jest.fn((euid: string): Observable<ValidationPayload> => {
    return new Observable(subscriber => {
      const valid = euid === VALID_EUID

      subscriber.next(
        new ValidationPayload(
          RegistrationNumberTypeEnum.EUID,
          euid,
          valid,
          new ValidationEvidence('https://validator.eu/checkEuidService', DateTime.now(), RegistrationNumberTypeEnum.EUID),
          'EUID validation error message'
        )
      )
      subscriber.complete()
    })
  })
}
