import { CacheModule } from '@nestjs/cache-manager'
import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import * as request from 'supertest'

import * as packageJson from '../package.json'
import { AppController } from './app.controller'
import { OntologyVersionProvider } from './common/provider/ontology-version.provider'
import { ConfigServiceMock } from './common/test/config-service.mock'

describe('AppController', () => {
  const minPort = 54000
  const maxPort = 55000

  let port: number
  let app: NestApplication

  beforeAll(async () => {
    port = Math.floor(Math.random() * (maxPort - minPort + 1) + minPort)

    const moduleRef = await Test.createTestingModule({
      imports: [CacheModule.register()],
      controllers: [AppController],
      providers: [ConfigService, OntologyVersionProvider]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: `http://127.0.0.1:${port}/main`,
          ONTOLOGY_VERSION: 'my-version'
        })
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.listen(port)
  })

  afterAll(async () => {
    await app.close()
  })

  it('should return the application information', async () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect({
        software: packageJson.name,
        description: packageJson.description,
        version: packageJson.version,
        ontologyVersion: 'my-version',
        documentation: `http://127.0.0.1:${port}/main/docs/`,
        repository: packageJson.repository.url,
        bugs: packageJson.bugs.url,
        features: {
          taxIDSupported: false
        }
      })
  })
})
