import { MiddlewareConsumer } from '@nestjs/common'
import { MiddlewareConfigProxy } from '@nestjs/common/interfaces'
import { ConfigService } from '@nestjs/config'

import { AppModule } from './app.module'
import { ConfigServiceMock } from './common/test/config-service.mock'
import { RequestLoggerMiddleware } from './middleware/request-logger.middleware'

describe('AppModule', () => {
  const consumer: MiddlewareConsumer = {
    apply: jest.fn()
  } as unknown as MiddlewareConsumer

  const middlewareConfig: MiddlewareConfigProxy = {
    forRoutes: jest.fn()
  } as unknown as MiddlewareConfigProxy

  beforeEach(() => {
    jest.resetAllMocks()
    jest.spyOn(consumer, 'apply').mockReturnValue(middlewareConfig)
    jest.spyOn(middlewareConfig, 'forRoutes').mockReturnValue(jest.fn())
  })

  it.each([
    ['true', 'true'],
    ['true', 'false'],
    ['false', 'true']
  ])('should apply request logger middleware (DEBUG_REQUESTS: %p, DEBUG_RESPONSES: %p)', (debugRequests, debugResponses) => {
    const appModule = new AppModule(
      new ConfigServiceMock({
        DEBUG_REQUESTS: debugRequests,
        DEBUG_RESPONSES: debugResponses
      }) as unknown as ConfigService
    )

    appModule.configure(consumer)

    expect(consumer.apply).toHaveBeenCalledWith(RequestLoggerMiddleware)
    expect(middlewareConfig.forRoutes).toHaveBeenCalledWith('*')
  })

  it('should skip applying the request logger middleware', () => {
    const appModule = new AppModule(
      new ConfigServiceMock({
        DEBUG_REQUESTS: 'false',
        DEBUG_RESPONSES: 'false'
      }) as unknown as ConfigService
    )

    appModule.configure(consumer)

    expect(consumer.apply).not.toHaveBeenCalled()
    expect(middlewareConfig.forRoutes).not.toHaveBeenCalled()
  })
})
