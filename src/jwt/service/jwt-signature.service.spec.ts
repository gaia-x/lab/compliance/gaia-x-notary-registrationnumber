import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'
import { decodeProtectedHeader, importSPKI, jwtVerify } from 'jose'

import { CommonModule } from '../../common/common.module'
import { PRIVATE_KEY_ALGORITHM, PUBLIC_KEY } from '../../common/constants'
import { PrivateKeyService } from '../../common/service/private-key.service'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { DidWebProvider } from '../../identity/provider/did-web.provider'
import { DidService } from '../../identity/service/did.service'
import { JwtSignatureService } from './jwt-signature.service'

describe('JwtSignatureService', () => {
  let jwtSignatureService: JwtSignatureService
  let mockConfigService: ConfigServiceMock

  beforeAll(async () => {
    mockConfigService = new ConfigServiceMock({
      BASE_URL: 'https://gaia-x.eu/main',
      OFFLINE_CONTEXTS: 'true',
      ONTOLOGY_VERSION: 'development'
    }).withKeyPairAndCertificate()
    const moduleFixture: TestingModule = await Test.createTestingModule({
      providers: [JwtSignatureService, PrivateKeyService, DidService, DidWebProvider],
      imports: [CommonModule, ConfigModule]
    })
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile()

    jwtSignatureService = moduleFixture.get<JwtSignatureService>(JwtSignatureService)
    moduleFixture.init()
  })

  it('should be defined', () => {
    expect(jwtSignatureService).toBeDefined()
  })

  it('should return a JWT object from a VerifiableCredential', async () => {
    const jwt = await jwtSignatureService.signVerifiableCredential({})
    expect(jwt).toBeDefined()
    expect(jwt).not.toHaveLength(0)
  })

  it('should return JWT containing iss and kid headers', async () => {
    const jwt = await jwtSignatureService.signVerifiableCredential({})
    const headers = decodeProtectedHeader(jwt)
    expect(headers).toBeDefined()
    expect(headers.kid).not.toHaveLength(0)
    expect(headers.iss).toEqual('did:web:gaia-x.eu:main')
  })

  it('should return JWT containing the VerifiableCredential as payload', async () => {
    const verifiableCredential = {
      '@context': ['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'],
      type: ['VerifiableCredential'],
      id: 'https://example.org/credentials/123',
      name: 'VAT ID',
      description: 'Value Added Tax Identifier',
      issuer: 'did:web:registrationnumber.notary.lab.gaia-x.eu:development',
      validFrom: '2024-07-15T09:20:56.971+00:00',
      validUntil: '2024-10-13T09:20:56.972+00:00',
      credentialSubject: {
        id: 'https://example.org/subjects/123',
        type: 'gx:VatID',
        'gx:vatID': 'BE0762747721',
        'gx:countryCode': 'BE'
      },
      evidence: {
        'gx:evidenceOf': 'VAT_ID',
        'gx:evidenceURL': 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService',
        'gx:executionDate': '2024-07-15T09:20:56.971+00:00'
      }
    }
    const jwt = await jwtSignatureService.signVerifiableCredential(verifiableCredential)

    const publicKey = await importSPKI(mockConfigService.get(PUBLIC_KEY), mockConfigService.get(PRIVATE_KEY_ALGORITHM))
    const results = await jwtVerify(jwt, publicKey)
    expect(results).toBeDefined()
    expect(results.payload).toBeDefined()
    expect(results.payload.id).toBeDefined()
    expect(results.payload.credentialSubject).toBeDefined()
    expect((<any>results.payload.credentialSubject).id).toBeDefined()
    expect((<any>results.payload)['evidence']).toBeDefined()
  })
})
