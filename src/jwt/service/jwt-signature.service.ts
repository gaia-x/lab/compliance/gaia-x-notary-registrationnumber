import { Inject, Injectable } from '@nestjs/common'
import { SignJWT } from 'jose'

import { CertificateService } from '../../common/service/certificate.service'
import { PrivateKeyService } from '../../common/service/private-key.service'
import { DidService } from '../../identity/service/did.service'

@Injectable()
export class JwtSignatureService {
  private readonly CTY_VALUE = 'vc+ld'
  private readonly TYP_VALUE = 'vc+ld+jwt'

  constructor(
    @Inject('DidWeb') private readonly didWeb: string,
    private readonly privateKeyService: PrivateKeyService,
    private readonly didService: DidService,
    private readonly certificateService: CertificateService
  ) {}

  async signVerifiableCredential(verifiableCredential: any): Promise<string> {
    verifiableCredential.validUntil = await this.certificateService.computeCredentialExpirationDate()
    const instance = new SignJWT(verifiableCredential).setProtectedHeader({
      alg: this.privateKeyService.getPrivateKeyAlgorithm(),
      iss: this.didWeb,
      kid: `${this.didWeb}#${this.didService.getVerificationMethodIdentifier()}`,
      iat: new Date(verifiableCredential.validFrom).getTime(),
      exp: new Date(verifiableCredential.validUntil).getTime(),
      cty: this.CTY_VALUE,
      typ: this.TYP_VALUE
    })
    return instance.sign(await this.privateKeyService.getPrivateKey())
  }
}
