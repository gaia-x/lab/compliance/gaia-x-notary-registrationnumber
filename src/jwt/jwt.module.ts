import { Module } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { CertificateService } from '../common/service/certificate.service'
import { PrivateKeyService } from '../common/service/private-key.service'
import { IdentityModule } from '../identity/identity.module'
import { JwtSignatureService } from './service/jwt-signature.service'

@Module({
  providers: [CommonModule, JwtSignatureService, PrivateKeyService, ConfigService, CertificateService],
  imports: [IdentityModule],
  exports: [JwtSignatureService]
})
export class JwtModule {}
