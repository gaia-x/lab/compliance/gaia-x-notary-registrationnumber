import { FactoryProvider, Injectable, InjectionToken } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import * as jose from 'jose'
import { KeyLike } from 'jose'

import { PRIVATE_KEY, PRIVATE_KEY_ALGORITHM, PS256 } from '../common/constants'

@Injectable()
export class JosePrivateKeyProvider implements FactoryProvider {
  readonly provide = 'josePrivateKey'
  inject: Array<InjectionToken> = [ConfigService]

  async useFactory(configService: ConfigService): Promise<KeyLike> {
    return await jose.importPKCS8(configService.get<string>(PRIVATE_KEY), configService.get<string>(PRIVATE_KEY_ALGORITHM, PS256))
  }
}
