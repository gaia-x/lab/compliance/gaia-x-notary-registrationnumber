import { InMemoryStorageService } from '@gaia-x/oidc4vc'
import { Injectable } from '@nestjs/common'
import { Logger } from '@nestjs/common/services'
import { Cron } from '@nestjs/schedule'
import { CronExpression } from '@nestjs/schedule/dist'

@Injectable()
export class StorageService extends InMemoryStorageService {
  private readonly expireAfterMs: number

  constructor() {
    super()
    this.expireAfterMs = parseInt(process.env.STORAGE_EXPIRATION ?? '600') * 1000
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async deleteOldCredentials() {
    const oldest = new Date(Date.now() - this.expireAfterMs)
    const deleted = await this.deleteOlderThan(oldest)
    if (deleted) {
      Logger.debug(`Deleted ${deleted} outdated credentials`)
    }
  }
}
