import { OIDC4VCIService as BaseOIDC4VCIService, CredentialSupported } from '@gaia-x/oidc4vc'
import { Inject, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { KeyLike } from 'jose'

import { StorageService } from './storage.service'

const CREDENTIAL_SUPPORT: CredentialSupported[] = [
  {
    format: 'jwt_vc_json',
    id: 'gx:legalRegistrationNumber',
    types: ['VerifiableCredential', 'gx:legalRegistrationNumber'],
    display: [
      {
        name: 'Legal registration number',
        locale: 'en-US',
        logo: {
          url: 'https://www.systnaps.com/wp-content/uploads/2021/04/Gaia-X_Logo_Standard_RGB_Transparent_210401-e1617719976112.png',
          alt_text: 'The Gaia-X logo'
        },
        background_color: '#46daff',
        text_color: '#000094'
      }
    ]
  }
]

@Injectable()
export class OIDC4VCIService extends BaseOIDC4VCIService {
  constructor(@Inject('josePrivateKey') privateKey: KeyLike, storageService: StorageService, configService: ConfigService) {
    super(privateKey, CREDENTIAL_SUPPORT, {
      baseUrl: configService.get('BASE_URL'),
      deleteOnUse: true,
      storageService
    })
  }
}
