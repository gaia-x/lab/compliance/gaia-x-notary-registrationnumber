import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { Oidc4VCIController } from './oidc4vci.controller'
import { OIDC4VCIService } from './oidc4vci.service'
import { JosePrivateKeyProvider } from './private-key.provider'
import { JosePublicKeyProvider } from './public-key.provider'
import { StorageService } from './storage.service'

@Module({
  imports: [CommonModule, ConfigModule],
  controllers: [Oidc4VCIController],
  providers: [OIDC4VCIService, StorageService, new JosePrivateKeyProvider(), new JosePublicKeyProvider()],
  exports: [OIDC4VCIService]
})
export class OIDC4VCIModule {}
