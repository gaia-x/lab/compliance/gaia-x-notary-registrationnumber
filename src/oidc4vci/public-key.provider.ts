import { FactoryProvider, Injectable, InjectionToken } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import * as jose from 'jose'
import { KeyLike } from 'jose'

import { PRIVATE_KEY_ALGORITHM, PS256, X509_CERTIFICATE } from '../common/constants'

@Injectable()
export class JosePublicKeyProvider implements FactoryProvider {
  readonly provide = 'josePublicKey'
  inject: Array<InjectionToken> = [ConfigService]

  async useFactory(configService: ConfigService): Promise<KeyLike> {
    return jose.importX509(configService.get<string>(X509_CERTIFICATE), configService.get(PRIVATE_KEY_ALGORITHM, PS256))
  }
}
