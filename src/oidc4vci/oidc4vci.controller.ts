import { PreAuthorizedCodeUtils } from '@gaia-x/oidc4vc'
import { Body, Controller, Get, Header, Headers, HttpCode, Inject, Param, Post } from '@nestjs/common'
import { ApiBody, ApiExcludeController, ApiOperation, ApiTags } from '@nestjs/swagger'
import {
  AccessTokenRequest,
  AccessTokenResponse,
  CommonCredentialRequest,
  CredentialIssuerMetadataOpts,
  CredentialOfferPayloadV1_0_11,
  CredentialRequestJwtVcJson,
  CredentialResponse
} from '@sphereon/oid4vci-common'
import { KeyLike } from 'jose'

import { OIDC4VCIService } from './oidc4vci.service'

@ApiExcludeController()
@ApiTags('oidc4vci')
@Controller()
export class Oidc4VCIController {
  constructor(
    @Inject('josePublicKey') private readonly publicKey: KeyLike,
    private readonly oidc4vciService: OIDC4VCIService
  ) {}

  @ApiOperation({
    summary: 'Retrieve issuer OpenId for Verifiable Credentials issuance issuer metadata'
  })
  @Get(['.well-known/openid-credential-issuer', 'auth/.well-known/openid-configuration'])
  @Header('Content-Type', 'application/json')
  getCredentialIssuerMetadata(): CredentialIssuerMetadataOpts {
    return this.oidc4vciService.generateIssuerMetadata()
  }

  @ApiOperation({
    summary: 'Request an access token'
  })
  @ApiBody({
    description: 'An access token request'
  })
  @Post('token')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  requestToken(@Body() accessTokenRequest: AccessTokenRequest): Promise<AccessTokenResponse> {
    return this.oidc4vciService.issueAccessToken(accessTokenRequest)
  }

  @ApiOperation({ summary: 'Request a credential offer from an authorization code' })
  @Get('credentialOffer/:preAuthorizationCode')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  async getCredentialOffer(@Param('preAuthorizationCode') preAuthorizedCode: string): Promise<CredentialOfferPayloadV1_0_11> {
    const credentialOfferSession = await this.oidc4vciService.getCredentialOffer(preAuthorizedCode)
    if (!credentialOfferSession) {
      throw new Error('Session expired, please renew it')
    }
    return credentialOfferSession.credentialOffer.credential_offer
  }

  @ApiOperation({
    summary: 'Present a credential from the given request'
  })
  @ApiBody({ description: 'A credential request' })
  @Post('credential')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  async issueCredential(
    @Headers('Authorization') authz: string,
    @Body()
    credentialRequest: CommonCredentialRequest & CredentialRequestJwtVcJson
  ): Promise<CredentialResponse> {
    const preAuthorizedCode: string = await PreAuthorizedCodeUtils.extractFromJwt(authz, this.publicKey)
    return this.oidc4vciService.issueCredential(preAuthorizedCode, credentialRequest)
  }
}
