import { CacheModule } from '@nestjs/cache-manager'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'

import { AppController } from './app.controller'
import { OntologyVersionProvider } from './common/provider/ontology-version.provider'

describe('AppController', () => {
  it.each([undefined, null, '', ' '])('should throw and error when base URL is missing', (baseUrl: string) => {
    const configService: ConfigService = new ConfigService()
    jest.spyOn(configService, 'get').mockReturnValue(baseUrl)

    expect(() => new AppController(configService, 'development')).toThrow(
      `Please provide this service's base url through the BASE_URL environment variable`
    )
  })
})
describe('AppController Integration', () => {
  let appController: AppController

  beforeEach(async () => {
    process.env.BASE_URL = 'https://localhost:3000/main'
    const moduleRef = await Test.createTestingModule({
      controllers: [AppController],
      imports: [ConfigModule, CacheModule.register()],
      providers: [OntologyVersionProvider]
    }).compile()
    appController = moduleRef.get(AppController)
  })

  it('should say taxIDSupported when OPEN_CORPORATES_API_KEY present', () => {
    process.env.OPEN_CORPORATES_VALIDATION_API_KEY = 'apiKeyValue'
    expect(appController.getApplicationInformation().features.taxIDSupported).toBeTruthy()
  })
  it('should say taxIDSupported false when OPEN_CORPORATES_API_KEY is empty', () => {
    process.env.OPEN_CORPORATES_VALIDATION_API_KEY = ''
    expect(appController.getApplicationInformation().features.taxIDSupported).toBeFalsy()
  })
  it('should say taxIDSupported false when OPEN_CORPORATES_API_KEY is not set', () => {
    delete process.env.OPEN_CORPORATES_VALIDATION_API_KEY
    expect(appController.getApplicationInformation().features.taxIDSupported).toBeFalsy()
  })

  it('should return development ontology version by default', () => {
    expect(appController.getApplicationInformation().ontologyVersion).toEqual('development')
  })
})
