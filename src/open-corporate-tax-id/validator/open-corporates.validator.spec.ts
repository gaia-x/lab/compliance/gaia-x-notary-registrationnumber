import { HttpModule, HttpService } from '@nestjs/axios'
import { Logger } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'
import { INVALID_TAX_ID, VALID_TAX_ID } from '../test/open-corporates-tax-id-validator.mock'
import { OpenCorporatesTaxIdValidator } from './open-corporates-tax-id-validator'

describe('OpenCorporatesValidator constructor', () => {
  it.each(['', undefined, null, ' '])('should throw an error when OPEN_CORPORATES_VALIDATION_API is missing', openCorporatesApiUrl => {
    const configService: ConfigService = new ConfigService()
    const httpService: HttpService = new HttpService()

    jest.spyOn(configService, 'get').mockImplementation(() => openCorporatesApiUrl)

    expect(() => new OpenCorporatesTaxIdValidator(configService, httpService)).toThrow(
      'Please provide the Open Corporates validation service URL through OPEN_CORPORATES_VALIDATION_API'
    )
  })

  it.each(['', undefined, null, ' '])('should not throw an error when OPEN_CORPORATES_VALIDATION_API_KEY is missing', openCorporatesApiKey => {
    const configService: ConfigService = new ConfigService()
    const httpService: HttpService = new HttpService()

    jest.spyOn(configService, 'get').mockImplementationOnce(() => 'https://api.opencorporates.com/v0.4/companies')
    jest.spyOn(configService, 'get').mockImplementation(() => openCorporatesApiKey)

    expect(() => new OpenCorporatesTaxIdValidator(configService, httpService)).not.toThrow()
  })
})

describe('OpenCorporatesValidator validation', () => {
  const openCorporatesApiUrl = 'https://api.opencorporates.com/v0.4/companies'

  let openCorporatesValidator: OpenCorporatesTaxIdValidator

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [OpenCorporatesTaxIdValidator]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          OPEN_CORPORATES_VALIDATION_API: openCorporatesApiUrl
        })
      )
      .setLogger(new Logger())
      .compile()

    openCorporatesValidator = moduleRef.get<OpenCorporatesTaxIdValidator>(OpenCorporatesTaxIdValidator)
  })

  it('should fail gracefully when the API KEY is not provided', () => {
    expect(() => openCorporatesValidator.validate(VALID_TAX_ID)).toThrow(
      'Validating the "schema:taxID" type is not supported by this Clearing House node'
    )
  })
})

describe('OpenCorporatesValidator', () => {
  const openCorporatesApiUrl = 'https://api.opencorporates.com/v0.4/companies'
  const openCorporatesApiKey = 'mockApiKey'

  let axiosMock: MockAdapter
  let openCorporatesValidator: OpenCorporatesTaxIdValidator

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [OpenCorporatesTaxIdValidator]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          OPEN_CORPORATES_VALIDATION_API: openCorporatesApiUrl,
          OPEN_CORPORATES_VALIDATION_API_KEY: openCorporatesApiKey
        })
      )
      .setLogger(new Logger())
      .compile()

    openCorporatesValidator = moduleRef.get<OpenCorporatesTaxIdValidator>(OpenCorporatesTaxIdValidator)
    axiosMock = new MockAdapter(axios)
  })

  it('should validate the registration number against the OpenCorporates API', done => {
    axiosMock.onGet(openCorporatesApiUrl).reply(
      200,
      `{
                "api_version": "0.4",
                "results": {
                    "companies": [
                        {
                            "company": {
                                "company_number": "0762747721",
                                "jurisdiction_code": "be",
                                "name": "GAIA-X European Association for Data and Cloud",
                                "opencorporates_url": "https://opencorporates.com/companies/be/0762747721"
                            }
                        }
                    ],
                    "page": 1,
                    "per_page": 30,
                    "total_count": 1,
                    "total_pages": 1
                }
            }`
    )

    openCorporatesValidator.validate(VALID_TAX_ID).subscribe({
      next(result: OpenCorporatesTaxIdValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.TAX_ID)
        expect(result.value).toEqual(VALID_TAX_ID)
        expect(result.valid).toEqual(true)
        expect(result.countryCode).toEqual('BE')
        expect(result.evidence.url).toEqual('https://opencorporates.com/companies/be/0762747721')
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.TAX_ID)
        expect(result.invalidationReason).toBeUndefined()

        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it.each(['', ' ', undefined, null])('should manage empty local registration numbers', emptyRegistrationNumber => {
    expect(() => openCorporatesValidator.validate(emptyRegistrationNumber)).toThrow('The local registration number was not provided')
  })

  it('should prefer native_company_number', done => {
    axiosMock.onGet(openCorporatesApiUrl).reply(
      200,
      `{
                "api_version": "0.4",
                "results": {
                    "companies": [
                        {
                            "company": {
                                "company_number": "M1201_HRB55399",
                                "native_company_number": "Frankfurt am Main HRB 55399",
                                "jurisdiction_code": "de",
                                "name": "GAIA-X European Association for Data and Cloud",
                                "opencorporates_url": "https://opencorporates.com/companies/de/M1201_HRB55399"
                            }
                        }
                    ],
                    "page": 1,
                    "per_page": 30,
                    "total_count": 1,
                    "total_pages": 1
                }
            }`
    )

    openCorporatesValidator.validate(VALID_TAX_ID).subscribe({
      next(result: OpenCorporatesTaxIdValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.TAX_ID)
        expect(result.value).toEqual('Frankfurt am Main HRB 55399')
        expect(result.valid).toEqual(true)
        expect(result.countryCode).toEqual('DE')
        expect(result.evidence.url).toEqual('https://opencorporates.com/companies/de/M1201_HRB55399')
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.TAX_ID)
        expect(result.invalidationReason).toBeUndefined()

        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage the case where the local registration number does not exist', done => {
    axiosMock.onGet(openCorporatesApiUrl).reply(
      200,
      `{
                "api_version": "0.4",
                "results": {
                    "companies": [],
                    "page": 1,
                    "per_page": 30,
                    "total_count": 0,
                    "total_pages": 1
                }
            }`
    )

    openCorporatesValidator.validate(INVALID_TAX_ID).subscribe({
      next(result: OpenCorporatesTaxIdValidationPayload) {
        assertFailed(result, `The TAX ID ${INVALID_TAX_ID} could not be found. Please check the input for typos.`)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage server errors', done => {
    axiosMock.onGet(openCorporatesApiUrl).reply(500)

    openCorporatesValidator.validate(INVALID_TAX_ID).subscribe({
      next(result: OpenCorporatesTaxIdValidationPayload) {
        assertFailed(result, `The TAX ID ${INVALID_TAX_ID} could not be validate due to an unknown error.`)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  function assertFailed(result: OpenCorporatesTaxIdValidationPayload, reason?: string) {
    expect(result.type).toEqual(RegistrationNumberTypeEnum.TAX_ID)
    expect(result.valid).toEqual(false)
    expect(result.value).toEqual(INVALID_TAX_ID)
    expect(result.countryCode).toEqual(null)
    expect(result.evidence.url).toEqual(openCorporatesApiUrl)
    expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
    expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.TAX_ID)

    expect(result.invalidationReason).toBeDefined()
    if (reason) {
      expect(result.invalidationReason).toEqual(reason)
    }
  }
})
