import { PreAuthorizedCodeUtils } from '@gaia-x/oidc4vc'
import { Controller, Get, Header, HttpCode, HttpException, HttpStatus, Inject, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiExcludeEndpoint,
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProduces,
  ApiQuery,
  ApiTags
} from '@nestjs/swagger'
import { lastValueFrom } from 'rxjs'

import { DocumentDto, VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { CertificateExpiryGuard } from '../../common/guard/certificate-expiry.guard'
import { RegistrationNumberInputGuard } from '../../common/guard/registration-number-input.guard'
import { DocumentMapper } from '../../common/mapper/document.mapper'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { OIDC4VCIService } from '../../oidc4vci/oidc4vci.service'
import { OpenCorporatesTaxIdCredentialSubjectDto } from '../dto/open-corporates-tax-id-credential-subject.dto'
import { OpenCorporatesTaxIdDocumentMapper } from '../mapper/open-corporates-tax-id-document.mapper'
import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'
import { OpenCorporatesTaxIdValidator } from '../validator/open-corporates-tax-id-validator'

@ApiTags('registration-number')
@Controller()
@UseGuards(CertificateExpiryGuard, RegistrationNumberInputGuard)
export class OpenCorporatesTaxIdController {
  private readonly documentMapper: DocumentMapper<OpenCorporatesTaxIdValidationPayload, OpenCorporatesTaxIdCredentialSubjectDto>

  constructor(
    @Inject('DidWeb') private readonly didWeb: string,
    @Inject('OntologyVersion') private readonly ontologyVersion: string,
    private readonly openCorporatesTaxIdValidator: OpenCorporatesTaxIdValidator,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly oidc4vciService: OIDC4VCIService
  ) {
    this.documentMapper = new OpenCorporatesTaxIdDocumentMapper(this.didWeb, ontologyVersion)
  }

  @Get('registration-numbers/tax-id/:taxId')
  @ApiOperation({
    summary: `Produce a registration number verifiable credential from an OpenCorporates Tax ID`,
    description: `This endpoint checks the given OpenCorporates Tax ID against the OpenCorporates API and responds with 
    a verifiable credential attesting the tax ID's validity as a registration number`
  })
  @HttpCode(200)
  @Header('Content-Type', 'application/vc+jwt')
  @ApiProduces('Content-Type', 'application/vc+jwt')
  @ApiQuery({
    name: 'vcId',
    description: `The output verifiable credential ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/credentials/123'
  })
  @ApiQuery({
    name: 'subjectId',
    description: `The output credential subject ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/subjects/123'
  })
  @ApiParam({
    name: 'taxId',
    description: `The OpenCorporates tax ID code to verify and produce the verifiable credential from`,
    type: 'string',
    required: true,
    example: '0762747721'
  })
  @ApiExtraModels(VerifiableCredentialDto)
  @ApiOkResponse({
    description: `Certified registration number verifiable credential`,
    content: {
      'application/vc+jwt': {
        schema: {
          type: 'string',
          description: 'JSON Web Token'
        },
        example:
          'eyJhbGciOiJQUzI1NiIsImlzcyI6ImRpZDp3ZWI6bG9jYWxob3N0OjMwMDAiLCJraWQiOiJkaWQ6d2ViOmxvY2FsaG9zdDozMDAwI1g1MDktSldLIiwiaWF0IjoxNzIxMDUxNjI1OTQzLCJleHAiOjE3Mjg4Mjc2MjU5NDMsImN0eSI6InZjK2xkIiwidHlwIjoidmMrbGQrand0In0.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9kZXZlbG9wbWVudCMiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvY3JlZGVudGlhbHMvMTIzIiwibmFtZSI6IlRheCBJRCIsImRlc2NyaXB0aW9uIjoiT3BlbiBDb3Jwb3JhdGVzIEFQSSBUYXggSUQiLCJpc3N1ZXIiOiJkaWQ6d2ViOmxvY2FsaG9zdDozMDAwIiwidmFsaWRGcm9tIjoiMjAyNC0wNy0xNVQxNTo1Mzo0NS45NDMrMDI6MDAiLCJ2YWxpZFVudGlsIjoiMjAyNC0xMC0xM1QxNTo1Mzo0NS45NDMrMDI6MDAiLCJjcmVkZW50aWFsU3ViamVjdCI6eyJAY29udGV4dCI6eyJzY2hlbWEiOiJodHRwczovL3NjaGVtYS5vcmcvIn0sImlkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZy9zdWJqZWN0cy8xMjMiLCJ0eXBlIjoiZ3g6VGF4SUQiLCJzY2hlbWE6dGF4SUQiOiIwNzYyNzQ3NzIxIn0sImV2aWRlbmNlIjp7Imd4OmV2aWRlbmNlT2YiOiJUQVhfSUQiLCJneDpldmlkZW5jZVVSTCI6Imh0dHBzOi8vb3BlbmNvcnBvcmF0ZXMuY29tL2NvbXBhbmllcy9iZS8wNzYyNzQ3NzIxIiwiZ3g6ZXhlY3V0aW9uRGF0ZSI6IjIwMjQtMDctMTVUMTU6NTM6NDUuOTQzKzAyOjAwIn19.ERDw7E8cZeCMF1KOP2ZFQLkUFcWhMBPbJ2WY2gtsFgZvR3HddcOoycoPcj7IfQGhtEWHVNoGgbKfYnbKaTeaoESy-1E5oPGUvNmXSk3XICbpNTFPiYyPdCICzhqzOn7icp9dYo8G2VFnY7CPB-z8zpb_yD43plzYePc7QLJkEJclTK9ZVN7cZ3dLCbzzwxe_68ZFSWGfc9D5MU56Ktd_sK8IpkVHplf9rmRJIvVXt8wOE5kRNxdBP5TrXyy2ZTgWv29KkUBRnIL1pnCdJAsDOmqK47N84HSWBxvw7OIEaOLoWuPzdLaRq48ukeycwmjms0xbs8cgj7mGg20wLVCbHQ'
      }
    }
  })
  @ApiBadRequestResponse({
    description: `Invalid legal registration number verifiable credential`,
    content: {
      'application/json': {
        examples: {
          'Payload missing': {
            value: {
              statusCode: 400,
              message: 'The request payload is missing'
            }
          },
          'Verifiable credential ID missing': {
            value: {
              statusCode: 400,
              message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
            }
          },
          'Subject ID missing': {
            value: {
              statusCode: 400,
              message: 'The subject ID is missing, please provide it through the subjectId query parameter'
            }
          },
          'Invalid registration number or API error': {
            value: {
              statusCode: 400,
              message: `The given taxID: [0762747721] couldn't be validated: error message`
            }
          }
        }
      }
    }
  })
  async checkTaxId(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('taxId') taxId: string): Promise<string> {
    const validationPayload: OpenCorporatesTaxIdValidationPayload = await lastValueFrom(this.openCorporatesTaxIdValidator.validate(taxId))

    if (!validationPayload.valid) {
      throw new HttpException(
        `The given taxID: [${taxId}] couldn't be validated: ${validationPayload.invalidationReason}`,
        HttpStatus.BAD_REQUEST
      )
    }

    const document: DocumentDto = this.documentMapper.mapFromValidationPayload(vcId, subjectId, validationPayload)

    return this.jwtSignatureService.signVerifiableCredential(document)
  }

  @ApiExcludeEndpoint()
  @Get('registration-numbers/tax-id/:taxId/credential')
  async issueTaxId(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('taxId') taxId: string) {
    const signedDocument = await this.checkTaxId(vcId, subjectId, taxId)
    // @ts-expect-error Will be fixed when @gaia-x/oidc4vc support https://www.w3.org/TR/vc-data-model-2.0/
    return await this.oidc4vciService.createCredentialOffer(PreAuthorizedCodeUtils.generate(), signedDocument)
  }
}
