import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { JwtSignatureService } from '../jwt/service/jwt-signature.service'
import { OIDC4VCIModule } from '../oidc4vci/oidc4vci.module'
import { OpenCorporatesTaxIdController } from './controller/open-corporates-tax-id.controller'
import { OpenCorporatesTaxIdValidator } from './validator/open-corporates-tax-id-validator'

@Module({
  imports: [CommonModule, ConfigModule, HttpModule, OIDC4VCIModule],
  controllers: [OpenCorporatesTaxIdController],
  providers: [OpenCorporatesTaxIdValidator, JwtSignatureService]
})
export class OpenCorporatesTaxIdModule {}
