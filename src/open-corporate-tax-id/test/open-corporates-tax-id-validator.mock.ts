import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'

export const VALID_TAX_ID: string = '0762747721'
export const INVALID_TAX_ID: string = '0123456789'

export const OpenCorporatesTaxIdValidatorMock = {
  validate: jest.fn((registrationNumber: string): Observable<OpenCorporatesTaxIdValidationPayload> => {
    return new Observable(subscriber => {
      const valid = registrationNumber === VALID_TAX_ID

      subscriber.next(
        new OpenCorporatesTaxIdValidationPayload(
          registrationNumber,
          valid,
          'BE',
          new ValidationEvidence('https://opencorporates.com/companies/be/0762747721', DateTime.now(), RegistrationNumberTypeEnum.TAX_ID),
          'TaxId validation error message'
        )
      )
      subscriber.complete()
    })
  })
}
