import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from '../../common/dto/credential-subject.dto'

export class OpenCorporatesTaxIdCredentialSubjectDto extends CredentialSubjectDto {
  @ApiProperty()
  'schema:taxID': string
}
