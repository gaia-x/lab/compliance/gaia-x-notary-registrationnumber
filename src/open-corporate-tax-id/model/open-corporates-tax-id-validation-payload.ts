import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { ValidationPayload } from '../../common/validator/model/validation-payload'

export class OpenCorporatesTaxIdValidationPayload extends ValidationPayload {
  constructor(
    value: string,
    valid: boolean,
    private readonly _countryCode: string,
    evidence: ValidationEvidence,
    invalidationReason?: string
  ) {
    super(RegistrationNumberTypeEnum.TAX_ID, value, valid, evidence, invalidationReason)
  }

  get countryCode(): string {
    return this._countryCode
  }
}
