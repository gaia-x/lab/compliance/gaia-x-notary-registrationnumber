import { HttpException, HttpStatus } from '@nestjs/common'

export class OpenCorporatesNotConfiguredException extends HttpException {
  constructor() {
    // Raise 4xx since it's user's fault for requesting a validation type that's not supported.
    // 5xx usually means retry will solve it
    super('Validating the "schema:taxID" type is not supported by this Clearing House node', HttpStatus.BAD_REQUEST)
  }
}
