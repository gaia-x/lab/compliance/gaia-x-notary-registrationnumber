import { CredentialSubjectMapper } from '../../common/mapper/credential-subject.mapper'
import { OpenCorporatesTaxIdCredentialSubjectDto } from '../dto/open-corporates-tax-id-credential-subject.dto'
import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'

export class OpenCorporatesTaxIdCredentialSubjectMapper
  implements CredentialSubjectMapper<OpenCorporatesTaxIdValidationPayload, OpenCorporatesTaxIdCredentialSubjectDto>
{
  map(payload: OpenCorporatesTaxIdValidationPayload, subjectId: string): OpenCorporatesTaxIdCredentialSubjectDto {
    return {
      '@context': {
        schema: 'https://schema.org/'
      },
      id: subjectId,
      type: 'gx:TaxID',
      'schema:taxID': payload.value
    }
  }
}
