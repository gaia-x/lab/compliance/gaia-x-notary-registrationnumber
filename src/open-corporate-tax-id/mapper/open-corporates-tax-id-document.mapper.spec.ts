import { DateTime } from 'luxon'

import { DocumentDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'
import { VALID_TAX_ID } from '../test/open-corporates-tax-id-validator.mock'
import { OpenCorporatesTaxIdDocumentMapper } from './open-corporates-tax-id-document.mapper'

describe('OpenCorporatesTaxIdDocumentMapper', () => {
  it('should map an Open Corporates tax ID validation payload to a document', () => {
    const mapper: OpenCorporatesTaxIdDocumentMapper = new OpenCorporatesTaxIdDocumentMapper('did:web:gaia-x.eu', 'development')

    const document: DocumentDto = mapper.mapFromValidationPayload(
      'https://example.org/credentials/123',
      'https://example.org/subjects/123',
      new OpenCorporatesTaxIdValidationPayload(
        VALID_TAX_ID,
        true,
        'FR',
        new ValidationEvidence('https://open-corporates.org/check/', DateTime.now(), RegistrationNumberTypeEnum.TAX_ID)
      )
    )

    expect(document['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
    expect(document.name).toEqual('Tax ID')
    expect(document.description).toEqual('Open Corporates API Tax ID')
  })
})
