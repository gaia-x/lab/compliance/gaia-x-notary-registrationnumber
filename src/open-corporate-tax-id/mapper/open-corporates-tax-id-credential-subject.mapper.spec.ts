import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'
import { VALID_TAX_ID } from '../test/open-corporates-tax-id-validator.mock'
import { OpenCorporatesTaxIdCredentialSubjectMapper } from './open-corporates-tax-id-credential-subject.mapper'

describe('OpenCorporatesTaxIdCredentialSubjectMapper', () => {
  it('should map a tax ID validation payload to a credential subject', () => {
    const mapper: OpenCorporatesTaxIdCredentialSubjectMapper = new OpenCorporatesTaxIdCredentialSubjectMapper()
    const validationPayload: OpenCorporatesTaxIdValidationPayload = new OpenCorporatesTaxIdValidationPayload(VALID_TAX_ID, true, 'FR', null)

    expect(mapper.map(validationPayload, 'https://example.org/subject/123')).toEqual({
      '@context': {
        schema: 'https://schema.org/'
      },
      id: 'https://example.org/subject/123',
      type: 'gx:TaxID',
      'schema:taxID': VALID_TAX_ID
    })
  })
})
