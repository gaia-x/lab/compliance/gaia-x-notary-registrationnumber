import { DocumentMapper } from '../../common/mapper/document.mapper'
import { OpenCorporatesTaxIdCredentialSubjectDto } from '../dto/open-corporates-tax-id-credential-subject.dto'
import { OpenCorporatesTaxIdValidationPayload } from '../model/open-corporates-tax-id-validation-payload'
import { OpenCorporatesTaxIdCredentialSubjectMapper } from './open-corporates-tax-id-credential-subject.mapper'

export class OpenCorporatesTaxIdDocumentMapper extends DocumentMapper<OpenCorporatesTaxIdValidationPayload, OpenCorporatesTaxIdCredentialSubjectDto> {
  constructor(didWeb: string, ontologyVersion: string) {
    super(didWeb, ontologyVersion, new OpenCorporatesTaxIdCredentialSubjectMapper(), 'Tax ID', 'Open Corporates API Tax ID')
  }
}
