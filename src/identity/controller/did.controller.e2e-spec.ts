import { CacheModule } from '@nestjs/cache-manager'
import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import * as jose from 'jose'
import { pki } from 'node-forge'
import * as request from 'supertest'

import { CertificateBuilder } from '../../common/test/certificate-builder'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { KeyBuilder } from '../../utils/key-builder'
import { IdentityModule } from '../identity.module'

describe('DidController', () => {
  const minPort = 54000
  const maxPort = 55000

  let certificatePem: string
  let port: number
  let app: NestApplication

  beforeAll(async () => {
    const certificate = CertificateBuilder.createCertificate(KeyBuilder.buildKeyPair())
    certificatePem = pki.certificateToPem(certificate)

    port = Math.floor(Math.random() * (maxPort - minPort + 1) + minPort)

    const moduleRef = await Test.createTestingModule({
      imports: [CacheModule.register({ isGlobal: true }), IdentityModule]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: `http://127.0.0.1:${port}/main`,
          X509_CERTIFICATE: certificatePem
        })
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.listen(port)
  })

  afterAll(async () => {
    await app.close()
  })

  it.each(['/did.json', '/.well-known/did.json'])('should return a valid did.json identity', async (url: string) => {
    const certChain = await jose.importX509(certificatePem, 'PS256')

    return request(app.getHttpServer())
      .get(url)
      .expect(200)
      .expect({
        '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/jwk/v1'],
        id: `did:web:127.0.0.1:${port}:main`,
        verificationMethod: [
          {
            id: `did:web:127.0.0.1:${port}:main#X509-JWK`,
            type: 'JsonWebKey',
            controller: `did:web:127.0.0.1:${port}:main`,
            publicKeyJwk: {
              kid: 'X509-JWK',
              ...(await jose.exportJWK(certChain)),
              alg: 'PS256',
              x5u: `http://127.0.0.1:${port}/main/.well-known/x509CertificateChain.pem`
            }
          }
        ],
        assertionMethod: [`did:web:127.0.0.1:${port}:main#X509-JWK`]
      })
  })
})
