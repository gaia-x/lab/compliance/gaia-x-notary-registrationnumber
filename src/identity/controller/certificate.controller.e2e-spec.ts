import { CacheModule } from '@nestjs/cache-manager'
import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import { pki } from 'node-forge'
import * as request from 'supertest'

import { CertificateBuilder } from '../../common/test/certificate-builder'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { KeyBuilder } from '../../utils/key-builder'
import { IdentityModule } from '../identity.module'

describe('CertificateController', () => {
  let certificate: pki.Certificate
  let app: NestApplication

  beforeAll(async () => {
    certificate = await CertificateBuilder.createCertificate(KeyBuilder.buildKeyPair())

    const moduleRef = await Test.createTestingModule({
      imports: [CacheModule.register({ isGlobal: true }), IdentityModule]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'http://127.0.0.1:3000/main',
          X509_CERTIFICATE: certificate.toString()
        })
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it.each(['/x509CertificateChain.pem', '/.well-known/x509CertificateChain.pem'])('should return the x509 certificate chain', async (url: string) =>
    request(app.getHttpServer()).get(url).expect(200).expect(certificate.toString())
  )
})
