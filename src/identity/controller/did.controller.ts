import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Header, UseInterceptors } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'

import { DidService } from '../service/did.service'

const didExample = {
  '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/jwk/v1'],
  id: 'did:web:registration.lab.gaia-x.eu:development',
  verificationMethod: [
    {
      id: 'did:web:registration.lab.gaia-x.eu:development#X509-JWK',
      type: 'JsonWebKey',
      controller: 'did:web:registration.lab.gaia-x.eu:development',
      publicKeyJwk: {
        kid: 'X509-JWK',
        kty: 'RSA',
        n: 'ulmXEa0nehbR338h6QaWLjMqfXE7mKA9PXoC_6_8d26xKQuBKAXa5k0uHhzQfNlAlxO-IpCDgf9cVzxIP-tkkefsjrXc8uvkdKNK6TY9kUxgUnOviiOLpHe88FB5dMTH6KUUGkjiPfq3P0F9fXHDEoQkGSpWui7eD897qSEdXFre_086ns3I8hSVCxoxlW9guXa_sRISIawCKT4UA3ZUKYyjtu0xRy7mRxNFh2wH0iSTQfqf4DWUUThX3S-jeRCRxqOGQdQlZoHym2pynJ1IYiiIOMO9L2IQrQl35kx94LGHiF8r8CRpLrgYXTVd9U17-nglrUmJmryECxW-555ppQ',
        e: 'AQAB',
        alg: 'PS256',
        x5u: 'https://registration.lab.gaia-x.eu/development/.well-known/x509CertificateChain.pem'
      }
    }
  ],
  assertionMethod: ['did:web:registration.lab.gaia-x.eu:development#X509-JWK']
}

@ApiTags('did')
@Controller()
@UseInterceptors(CacheInterceptor) // This allows to cache responses once they are built
export class DidController {
  constructor(private readonly didService: DidService) {}

  @Get(['did.json', '.well-known/did.json'])
  @ApiOperation({
    summary: 'Collect the Gaia-X Notary DID'
  })
  @Header('Content-Type', 'application/json')
  @ApiOkResponse({
    description: `The Gaia-X Notary's Decentralized Identifier (DID)`,
    content: {
      'application/json': {
        example: didExample
      }
    }
  })
  getDid(): Promise<string> {
    return this.didService.buildDid()
  }
}
