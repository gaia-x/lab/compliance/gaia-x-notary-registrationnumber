import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Header, UseInterceptors } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'

import { DidService } from '../service/did.service'

const certificateExample = `-----BEGIN CERTIFICATE-----
MIIEFDCCAvygAwIBAgIBATANBgkqhkiG9w0BAQUFADB9MRYwFAYDVQQDEw1kZXYu
Z2FpYS14LmV1MQswCQYDVQQGEwJGUjEbMBkGA1UECBMSTm91dmVsbGUtQXF1aXRh
aW5lMREwDwYDVQQHEwhCb3JkZWF1eDETMBEGA1UEChMKR2FpYS1YIERldjERMA8G
A1UECxMIQ1RPIFRlYW0wHhcNMjMxMjAxMDc1NjQyWhcNMjQxMjAxMDc1NjQyWjB9
MRYwFAYDVQQDEw1kZXYuZ2FpYS14LmV1MQswCQYDVQQGEwJGUjEbMBkGA1UECBMS
Tm91dmVsbGUtQXF1aXRhaW5lMREwDwYDVQQHEwhCb3JkZWF1eDETMBEGA1UEChMK
R2FpYS1YIERldjERMA8GA1UECxMIQ1RPIFRlYW0wggEiMA0GCSqGSIb3DQEBAQUA
A4IBDwAwggEKAoIBAQDPQOFYPyIBTKL+JfcUOGZn/2EQDhslgieN/qptgWP9o+Fr
o1YsnxIGXPtjQ0EhAOm7JH4W8AoeO9EMZ76P/GG1KT0cp+2IICaV2eUqFGYuUtIn
bo9h2aaj05DYvOEvrL9P2OuySDmw8f11Khm5fjSVi04/xyZNvPlV+wGIqd5glAnB
rtTAdC4zMxf0fewPdDA1pbAneo+TFWyWlTsGR0nlvmvqAgZj89c4luPCaio/P57i
ic4+Mt4wvOL5QeRwhFWJB4tINjXgv1lYfiycn/yGeeGuGFNxuY8N+d42Fola3YzK
ITYIRQtUkiM5hfPWgM0DrovUvw+hlKwcTK3ZfgFFAgMBAAGjgZ4wgZswDAYDVR0T
BAUwAwEB/zALBgNVHQ8EBAMCAvQwOwYDVR0lBDQwMgYIKwYBBQUHAwEGCCsGAQUF
BwMCBggrBgEFBQcDAwYIKwYBBQUHAwQGCCsGAQUFBwMIMBEGCWCGSAGG+EIBAQQE
AwIA9zAPBgNVHREECDAGhwR/AAABMB0GA1UdDgQWBBT1BvOfP1ecxrRFGtJWTXm2
d5/ALzANBgkqhkiG9w0BAQUFAAOCAQEASSepIDFBw5+ThbgKPhIXte8Rjbx0hNqT
qt/cplp3159K08l81H5v4O1QozS7zSTO4bV7RSFr/0FuSu9svBqqcl4rYqWa4n7w
WBg2ahaQ8Nx8MMHuoqsWzFugL60wVsRm1uXqo/EI1yCmTUpDCB8p1pS1MW6sq1GA
v2Ic4YxK+9X92epLKwx077UzYemLH8f6y/Y+BgwYliRRWEAwPoS9ZONeyxK8iSNm
AurH3CsZKaG+jpSObL2+mAkiZK/IyJV5LiX0eDpH9U0w7H/LEQFGs/mMFmnMZLgq
sOzDVjSUT3BWNSLZbN0uhap4MhLBFpCiZt8MYle18a2ZIQpOV9HKnQ==
-----END CERTIFICATE-----`

@ApiTags('certificate')
@Controller()
@UseInterceptors(CacheInterceptor)
export class CertificateController {
  constructor(private readonly didService: DidService) {}

  @Get(['x509CertificateChain.pem', '.well-known/x509CertificateChain.pem'])
  @ApiOperation({
    summary: 'Collect the Gaia-X Notary X509 certificate'
  })
  @Header('Content-Type', 'application/pem-certificate-chain')
  @ApiOkResponse({
    description: `The Gaia-X Notary's certificate`,
    content: {
      'application/pem-certificate-chain': {
        example: certificateExample
      }
    }
  })
  getCertChain(): string {
    return this.didService.getCertChain(DidService.FALLBACK_CERTIFICATE_PATH)
  }
}
