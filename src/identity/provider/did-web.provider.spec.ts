import { ConfigService } from '@nestjs/config'

import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { DidWebProvider } from './did-web.provider'

describe('DidWebProvider', () => {
  const provider = new DidWebProvider()

  it.each([
    ['http://localhost:3000/main', 'did:web:localhost:3000:main'],
    ['https://www.gaia-x.eu/', 'did:web:www.gaia-x.eu'],
    ['https://www.gaia-x.eu/main//', 'did:web:www.gaia-x.eu:main'],
    ['https://test.gaia-x.eu:1234', 'did:web:test.gaia-x.eu:1234']
  ])('should build a valid Did Web from base URL', (baseUrl, didWeb) => {
    expect(
      provider.useFactory(
        new ConfigServiceMock({
          BASE_URL: baseUrl
        }) as unknown as ConfigService
      )
    ).toEqual(didWeb)
  })

  it.each(['totally wrong', 'www.gaia-x.eu', 'www.gaia-x.eu/main', 'https://'])(
    'should throw an error when the base URL format is not compliant',
    baseUrl => {
      expect(() =>
        provider.useFactory(
          new ConfigServiceMock({
            BASE_URL: baseUrl
          }) as unknown as ConfigService
        )
      ).toThrow(`${baseUrl} doesn't match the required base URL format (ie. https://gaia-x.eu:1234/main)`)
    }
  )
})
