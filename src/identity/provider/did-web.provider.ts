import { FactoryProvider, InjectionToken } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

export class DidWebProvider implements FactoryProvider {
  private static readonly BASE_URL_REGEX = /^\w+:\/\/(.+?)\/*$/

  provide: InjectionToken = 'DidWeb'
  inject: Array<InjectionToken> = [ConfigService]

  useFactory(configService: ConfigService): string {
    const baseUrl = configService.get<string>('BASE_URL').trim()
    const matches = baseUrl.match(DidWebProvider.BASE_URL_REGEX)
    if (!matches || matches.length != 2) {
      throw new Error(`${baseUrl} doesn't match the required base URL format (ie. https://gaia-x.eu:1234/main)`)
    }

    return 'did:web:' + matches[1].replaceAll('/', ':')
  }
}
