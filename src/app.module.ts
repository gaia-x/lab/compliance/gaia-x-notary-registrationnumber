import { CacheModule } from '@nestjs/cache-manager'
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'

import { AppController } from './app.controller'
import { CommonModule } from './common/common.module'
import { EoriModule } from './eori/eori.module'
import { JwtModule } from './jwt/jwt.module'
import { LeiCodeModule } from './lei-code/lei-code.module'
import { RequestLoggerMiddleware } from './middleware/request-logger.middleware'
import { OIDC4VCIModule } from './oidc4vci/oidc4vci.module'
import { OpenCorporatesTaxIdModule } from './open-corporate-tax-id/open-corporates-tax-id.module'
import { VatIdModule } from './vat-id/vat-id.module'

@Module({
  imports: [
    ConfigModule.forRoot(),
    CacheModule.register(),
    ScheduleModule.forRoot(),
    CommonModule,
    VatIdModule,
    LeiCodeModule,
    JwtModule,
    EoriModule,
    OpenCorporatesTaxIdModule,
    // EuidModule, To be added when implemented
    OIDC4VCIModule
  ],
  controllers: [AppController],
  providers: []
})
export class AppModule implements NestModule {
  constructor(private readonly configService: ConfigService) {}

  configure(consumer: MiddlewareConsumer) {
    if (this.configService.get('DEBUG_REQUESTS') === 'true' || this.configService.get('DEBUG_RESPONSES') === 'true') {
      consumer.apply(RequestLoggerMiddleware).forRoutes('*')
    }
  }
}
