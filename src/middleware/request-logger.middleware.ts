import { Injectable, Logger, NestMiddleware } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NextFunction, Request, Response } from 'express'
import { inspect } from 'util'
import { v4 as uuidv4 } from 'uuid'

@Injectable()
export class RequestLoggerMiddleware implements NestMiddleware {
  private readonly logger = new Logger(RequestLoggerMiddleware.name)

  constructor(private readonly configService: ConfigService) {}

  use(req: Request, res: Response, next: NextFunction) {
    const uuid: string = uuidv4()

    if (this.configService.get('DEBUG_REQUESTS') === 'true') {
      this.logger.debug(`Request(${uuid}): ${req.ip} - ${req.method} - ${req.originalUrl} - ${inspect(req.body)}`)
    }

    if (this.configService.get('DEBUG_RESPONSES') === 'true') {
      this.logger.debug(`Response(${uuid}): ${res.statusCode}`)
    }

    next()
  }
}
