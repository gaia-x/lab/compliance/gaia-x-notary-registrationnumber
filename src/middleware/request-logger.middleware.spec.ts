import { ConfigService } from '@nestjs/config'
import { Request, Response } from 'express'

import { ConfigServiceMock } from '../common/test/config-service.mock'
import { RequestLoggerMiddleware } from './request-logger.middleware'

describe('RequestLoggerMiddleware', () => {
  it('should log requests', () => {
    const middleware: RequestLoggerMiddleware = new RequestLoggerMiddleware(
      new ConfigServiceMock({
        DEBUG_REQUESTS: 'true'
      }) as unknown as ConfigService
    )

    const request: Request = {
      ip: '127.0.0.1',
      method: 'GET',
      originalUrl: 'http://127.0.0.1:3000/vat-id?parameter=true',
      body: {
        myAttribute: 'my value',
        anotherAttribute: 'another value'
      }
    } as Request

    const next = jest.fn()
    middleware.use(request, null, next)

    expect(next).toHaveBeenCalled()
  })

  it('should log responses', () => {
    const middleware: RequestLoggerMiddleware = new RequestLoggerMiddleware(
      new ConfigServiceMock({
        DEBUG_RESPONSES: 'true'
      }) as unknown as ConfigService
    )

    const response: Response = {
      statusCode: 204
    } as Response

    const next = jest.fn()
    middleware.use(null, response, next)

    expect(next).toHaveBeenCalled()
  })
})
