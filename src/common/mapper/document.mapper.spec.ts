import { DateTime } from 'luxon'

import { VatIdCredentialSubjectDto } from '../../vat-id/dto/vat-id-credential-subject.dto'
import { VatIdDocumentMapper } from '../../vat-id/mapper/vat-id-document.mapper'
import { VatIdValidationPayload } from '../../vat-id/model/vat-id-validation-payload'
import { VALID_VAT_ID } from '../../vat-id/test/vat-id-validator.mock'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationEvidence } from '../validator/model/validation-evidence'
import { DocumentMapper } from './document.mapper'

describe('DocumentMapper', () => {
  const mapper: DocumentMapper<VatIdValidationPayload, VatIdCredentialSubjectDto> = new VatIdDocumentMapper('did:web:gaia-x.eu', 'development')
  const validationPayload: VatIdValidationPayload = new VatIdValidationPayload(
    VALID_VAT_ID,
    true,
    'FR',
    new ValidationEvidence('https://validator.eu/checkVatId', DateTime.fromISO('2024-05-30T17:08:32Z'), RegistrationNumberTypeEnum.VAT_ID)
  )

  it('should map a validation payload to a document DTO', () => {
    expect(mapper.mapFromValidationPayload('https://example.org/credentials/123', 'https://example.org/subjects/123', validationPayload)).toEqual({
      '@context': ['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'],
      type: ['VerifiableCredential', 'gx:VatID'],
      id: 'https://example.org/credentials/123',
      name: 'VAT ID',
      description: 'Value Added Tax Identifier',
      issuer: 'did:web:gaia-x.eu',
      validFrom: expect.any(String),
      validUntil: null,
      credentialSubject: {
        id: 'https://example.org/subjects/123',
        type: 'gx:VatID',
        'gx:vatID': 'FR0123456789',
        'gx:countryCode': 'FR'
      },
      evidence: {
        'gx:evidenceOf': RegistrationNumberTypeEnum.VAT_ID,
        'gx:evidenceURL': 'https://validator.eu/checkVatId',
        'gx:executionDate': expect.any(String)
      }
    })
  })
})
