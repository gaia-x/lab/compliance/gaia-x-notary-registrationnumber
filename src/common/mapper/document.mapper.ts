import { DateTime } from 'luxon'

import { CredentialSubjectDto } from '../dto/credential-subject.dto'
import { DocumentDto } from '../dto/verifiable-credential.dto'
import { ValidationPayload } from '../validator/model/validation-payload'
import { CredentialSubjectMapper } from './credential-subject.mapper'
import { EvidenceMapper } from './evidence.mapper'

export abstract class DocumentMapper<T extends ValidationPayload, O extends CredentialSubjectDto> {
  constructor(
    private readonly didWeb: string,
    private readonly ontologyVersion: string,
    private readonly credentialSubjectMapper: CredentialSubjectMapper<T, O>,
    private readonly vcName?: string,
    private readonly vcDescription?: string
  ) {}

  mapFromValidationPayload(vcId: string, subjectId: string, validationPayload: T): DocumentDto {
    return {
      '@context': ['https://www.w3.org/ns/credentials/v2', `https://w3id.org/gaia-x/${this.ontologyVersion}#`],
      type: ['VerifiableCredential', validationPayload.type],
      id: vcId,
      name: this.vcName,
      description: this.vcDescription,
      issuer: this.didWeb,
      validFrom: DateTime.now().toISO(),
      validUntil: null,
      credentialSubject: this.credentialSubjectMapper.map(validationPayload, subjectId),
      evidence: EvidenceMapper.map(validationPayload.evidence)
    }
  }
}
