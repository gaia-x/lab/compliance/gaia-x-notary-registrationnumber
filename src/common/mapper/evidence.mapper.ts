import { EvidenceDto } from '../dto/evidence.dto'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationEvidence } from '../validator/model/validation-evidence'

export class EvidenceMapper {
  static map(validationEvidence: ValidationEvidence): EvidenceDto {
    return {
      'gx:evidenceOf': validationEvidence.of,
      'gx:evidenceURL': validationEvidence.url,
      'gx:executionDate': validationEvidence.executionDate.toISO()
    }
  }
}
