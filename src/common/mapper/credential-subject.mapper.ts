import { CredentialSubjectDto } from '../dto/credential-subject.dto'
import { ValidationPayload } from '../validator/model/validation-payload'

export interface CredentialSubjectMapper<P extends ValidationPayload, O extends CredentialSubjectDto> {
  map(payload: P, subjectId: string): O
}
