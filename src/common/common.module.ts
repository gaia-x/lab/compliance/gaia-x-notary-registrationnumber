import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { IdentityModule } from '../identity/identity.module'
import { JwtSignatureService } from '../jwt/service/jwt-signature.service'
import { CertificateExpirationBatch } from './cron/certificate-expiration.batch'
import { OntologyVersionProvider } from './provider/ontology-version.provider'
import { CertificateService } from './service/certificate.service'
import { PrivateKeyService } from './service/private-key.service'

@Module({
  imports: [IdentityModule, ConfigModule, HttpModule],
  providers: [JwtSignatureService, PrivateKeyService, OntologyVersionProvider, CertificateExpirationBatch, CertificateService],
  exports: [IdentityModule, OntologyVersionProvider, CertificateService, PrivateKeyService]
})
export class CommonModule {}
