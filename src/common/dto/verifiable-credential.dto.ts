import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from './credential-subject.dto'
import { EvidenceDto } from './evidence.dto'

export type DocumentDto = Omit<VerifiableCredentialDto, 'proof'>

export class VerifiableCredentialDto {
  @ApiProperty()
  '@context': string | string[]

  @ApiProperty()
  type: string[]

  @ApiProperty()
  id: string

  @ApiProperty()
  name?: string

  @ApiProperty()
  description?: string

  @ApiProperty()
  issuer: string

  @ApiProperty()
  validFrom: string

  @ApiProperty()
  validUntil: string

  @ApiProperty()
  credentialSubject: CredentialSubjectDto

  @ApiProperty()
  evidence: EvidenceDto
}
