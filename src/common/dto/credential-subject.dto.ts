import { ApiProperty } from '@nestjs/swagger'

export abstract class CredentialSubjectDto {
  @ApiProperty()
  '@context'?: Record<string, string>

  @ApiProperty()
  type: string

  @ApiProperty()
  id: string
}
