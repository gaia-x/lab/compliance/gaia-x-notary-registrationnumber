import { ApiProperty } from '@nestjs/swagger'

export class EvidenceDto {
  @ApiProperty()
  'gx:evidenceURL': string

  @ApiProperty()
  'gx:executionDate': string

  @ApiProperty()
  'gx:evidenceOf': string
}
