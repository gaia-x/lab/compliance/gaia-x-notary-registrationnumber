import * as jsonld from 'jsonld'

import * as credentialContext from '../context/credentials_v2_context.json'
import * as jwkContext from '../context/jwk_context.json'
import * as schemaContext from '../context/schema_context.json'
import * as vcardContext from '../context/vcard_context.json'
import { DocumentLoader, RemoteDocument } from './jsonld.types'

export class OfflineDocumentLoader {
  private static readonly CONTEXTS: Record<string, any> = {
    'https://www.w3.org/ns/credentials/v2': credentialContext,
    'https://w3id.org/security/jwk/v1': jwkContext,
    'https://schema.org': schemaContext,
    'http://schema.org': schemaContext,
    'https://www.w3.org/2006/vcard/ns#': vcardContext,
    'http://www.w3.org/2006/vcard/ns#': vcardContext
  }

  static create(): DocumentLoader {
    const nodeDocumentLoader: DocumentLoader = jsonld.documentLoaders.node()
    return async (url: string): Promise<RemoteDocument> => {
      if (url in OfflineDocumentLoader.CONTEXTS) {
        return {
          contextUrl: null,
          document: OfflineDocumentLoader.CONTEXTS[url],
          documentUrl: url
        }
      }

      return nodeDocumentLoader(url)
    }
  }
}
