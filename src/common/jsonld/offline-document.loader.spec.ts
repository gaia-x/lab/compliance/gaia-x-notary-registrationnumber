import * as jsonld from 'jsonld'

import * as credentialContext from '../context/credentials_v2_context.json'
import * as jwkContext from '../context/jwk_context.json'
import * as schemaContext from '../context/schema_context.json'
import * as vcardContext from '../context/vcard_context.json'
import { DocumentLoader } from './jsonld.types'
import { OfflineDocumentLoader } from './offline-document.loader'

jest.mock('jsonld')

describe('OfflineDocumentLoader', () => {
  it('should resolve offline documents', async () => {
    const mockedDocumentLoader = jest.fn()
    ;(jsonld.documentLoaders.node as jest.Mock).mockReturnValue(mockedDocumentLoader)

    const documentLoader: DocumentLoader = OfflineDocumentLoader.create()

    expect(await documentLoader('https://www.w3.org/ns/credentials/v2')).toEqual({
      contextUrl: null,
      document: credentialContext,
      documentUrl: 'https://www.w3.org/ns/credentials/v2'
    })

    expect(await documentLoader('https://w3id.org/security/jwk/v1')).toEqual({
      contextUrl: null,
      document: jwkContext,
      documentUrl: 'https://w3id.org/security/jwk/v1'
    })

    expect(await documentLoader('https://schema.org')).toEqual({
      contextUrl: null,
      document: schemaContext,
      documentUrl: 'https://schema.org'
    })

    expect(await documentLoader('http://schema.org')).toEqual({
      contextUrl: null,
      document: schemaContext,
      documentUrl: 'http://schema.org'
    })

    expect(await documentLoader('https://www.w3.org/2006/vcard/ns#')).toEqual({
      contextUrl: null,
      document: vcardContext,
      documentUrl: 'https://www.w3.org/2006/vcard/ns#'
    })

    expect(await documentLoader('https://www.w3.org/2006/vcard/ns#')).toEqual({
      contextUrl: null,
      document: vcardContext,
      documentUrl: 'https://www.w3.org/2006/vcard/ns#'
    })

    expect((mockedDocumentLoader as jest.Mock).mock.calls).toHaveLength(0)
  })

  it(`should still call the default document loader when the URL isn't managed`, async () => {
    const mockedDocumentLoader = jest.fn(url => ({
      contextUrl: null,
      document: 'test value',
      documentUrl: url
    }))
    ;(jsonld.documentLoaders.node as jest.Mock).mockReturnValue(mockedDocumentLoader)

    const documentLoader: DocumentLoader = OfflineDocumentLoader.create()

    expect(await documentLoader('https://unmanaged.com/shape-registry/shape')).toEqual({
      contextUrl: null,
      document: 'test value',
      documentUrl: 'https://unmanaged.com/shape-registry/shape'
    })

    expect((mockedDocumentLoader as jest.Mock).mock.calls).toHaveLength(1)
    expect((mockedDocumentLoader as jest.Mock).mock.calls[0][0]).toEqual('https://unmanaged.com/shape-registry/shape')
  })
})
