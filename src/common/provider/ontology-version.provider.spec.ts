import { ConfigServiceMock } from '../test/config-service.mock'
import { OntologyVersionProvider } from './ontology-version.provider'

describe('OntologyVersionProvider', () => {
  it('should return development by default', () => {
    expect(OntologyVersionProvider.useFactory(new ConfigServiceMock({}))).toEqual('development')
  })

  it('should return a custom ontology version', () => {
    expect(OntologyVersionProvider.useFactory(new ConfigServiceMock({ ONTOLOGY_VERSION: '  my-version ' }))).toEqual('my-version')
  })
})
