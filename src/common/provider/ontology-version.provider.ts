import { FactoryProvider } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

export const OntologyVersionProvider: FactoryProvider<string> = {
  provide: 'OntologyVersion',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => configService.get<string>('ONTOLOGY_VERSION', 'development').trim()
}
