import { InternalServerErrorException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { CertificateService } from '../service/certificate.service'
import { ConfigServiceMock } from '../test/config-service.mock'
import { CertificateExpiryGuard } from './certificate-expiry.guard'

describe('CertificateExpiryGuard', () => {
  it('should allow to activate when certificate is valid', async () => {
    const configServiceMock = new ConfigServiceMock({}).withKeyPairAndCertificate() as unknown as ConfigService
    const certificateService = new CertificateService(configServiceMock)

    certificateService.onModuleInit()
    const guard: CertificateExpiryGuard = new CertificateExpiryGuard(certificateService)

    expect(await guard.canActivate()).toEqual(true)
  })

  it('should refuse to activate when certificate is expired', async () => {
    const configServiceMock = new ConfigServiceMock({}).withPrivateKeyAndExpiredCertificate() as unknown as ConfigService
    const certificateService = new CertificateService(configServiceMock)

    certificateService.onModuleInit()
    const guard: CertificateExpiryGuard = new CertificateExpiryGuard(certificateService)

    expect(guard.canActivate()).rejects.toThrow(
      new InternalServerErrorException('This instance certificate has expired and it will not be able to emit credentials')
    )
  })
})
