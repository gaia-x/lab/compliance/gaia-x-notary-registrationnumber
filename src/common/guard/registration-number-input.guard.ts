import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common'

@Injectable()
export class RegistrationNumberInputGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest()

    if (!request.query.vcId) {
      throw new HttpException(`The verifiable credential ID is missing, please provide it through the vcId query parameter`, HttpStatus.BAD_REQUEST)
    }

    if (!request.query.subjectId) {
      throw new HttpException(`The subject ID is missing, please provide it through the subjectId query parameter`, HttpStatus.BAD_REQUEST)
    }

    return true
  }
}
