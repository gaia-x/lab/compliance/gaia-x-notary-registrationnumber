import { CanActivate, Injectable, InternalServerErrorException } from '@nestjs/common'

import { CertificateService } from '../service/certificate.service'

@Injectable()
export class CertificateExpiryGuard implements CanActivate {
  constructor(private readonly certificateExpirationService: CertificateService) {}

  async canActivate(): Promise<boolean> {
    if (await this.certificateExpirationService.isCertificateExpired()) {
      throw new InternalServerErrorException('This instance certificate has expired and it will not be able to emit credentials')
    }

    return true
  }
}
