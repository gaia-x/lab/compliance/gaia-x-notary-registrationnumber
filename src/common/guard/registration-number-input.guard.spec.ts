import { ExecutionContext, HttpException, HttpStatus } from '@nestjs/common'

import { RegistrationNumberInputGuard } from './registration-number-input.guard'

describe('RegistrationNumberInputGuard', () => {
  const guard: RegistrationNumberInputGuard = new RegistrationNumberInputGuard()
  let executionContext: ExecutionContext
  let query

  beforeEach(() => {
    executionContext = {
      switchToHttp: () => ({
        getResponse: jest.fn(),
        getNext: jest.fn(),
        getRequest: () =>
          ({
            query
          }) as any
      })
    } as unknown as ExecutionContext
  })

  it('should allow to activate when vcId and subjectId are provided', () => {
    query = {
      vcId: 'https://example.org/credentials/123',
      subjectId: 'https://example.org/subjects/123'
    }

    expect(guard.canActivate(executionContext)).toBe(true)
  })

  it.each([
    {
      subjectId: 'https://example.org/subjects/123'
    },
    {}
  ])('should throw an error when vcId is not provided', inputQuery => {
    query = inputQuery

    expect(() => guard.canActivate(executionContext)).toThrow(
      new HttpException(`The verifiable credential ID is missing, please provide it through the vcId query parameter`, HttpStatus.BAD_REQUEST)
    )
  })

  it('should throw an error when subjectId is not provided', () => {
    query = {
      vcId: 'https://example.org/credentials/123'
    }

    expect(() => guard.canActivate(executionContext)).toThrow(
      new HttpException(`The subject ID is missing, please provide it through the subjectId query parameter`, HttpStatus.BAD_REQUEST)
    )
  })
})
