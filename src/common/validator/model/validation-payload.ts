import { RegistrationNumberTypeEnum } from '../../enum/registration-number-type.enum'
import { ValidationEvidence } from './validation-evidence'

/**
 * Represents the data returned by the validation endpoints
 */
export class ValidationPayload {
  constructor(
    public readonly type: RegistrationNumberTypeEnum,
    public readonly value: string,
    public readonly valid: boolean,
    public readonly evidence: ValidationEvidence,
    public readonly invalidationReason?: string
  ) {}
}
