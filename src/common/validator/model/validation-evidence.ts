import { DateTime } from 'luxon'

import { RegistrationNumberTypeEnum } from '../../enum/registration-number-type.enum'

export class ValidationEvidence {
  constructor(
    private readonly _url: string,
    private readonly _executionDate: DateTime,
    private readonly _of: RegistrationNumberTypeEnum
  ) {}

  get url(): string {
    return this._url
  }

  get executionDate(): DateTime {
    return this._executionDate
  }

  get of(): RegistrationNumberTypeEnum {
    return this._of
  }
}
