import { Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'

import { CREDENTIAL_LIFETIME_IN_DAYS } from '../constants'
import { CertificateService } from '../service/certificate.service'

@Injectable()
export class CertificateExpirationBatch {
  private readonly logger: Logger = new Logger(CertificateExpirationBatch.name)

  constructor(private readonly certificateExpirationService: CertificateService) {}

  @Cron(CronExpression.EVERY_12_HOURS)
  async checkCertificateValidity() {
    this.logger.log('Start of checkCertificateValidity batch')
    const remainingDays = await this.certificateExpirationService.getDaysBeforeCertExpirationDate()
    if (remainingDays < 0) {
      this.logger.error('[ALERT] The certificate used in this instance is expired. No credentials will be issued anymore')
    } else if (remainingDays < CREDENTIAL_LIFETIME_IN_DAYS) {
      this.logger.warn(`[ALERT] The certificate used in this instance will expire in ${remainingDays} days`)
    }
  }
}
