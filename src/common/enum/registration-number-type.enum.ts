export enum RegistrationNumberTypeEnum {
  EORI = 'gx:EORI',
  EUID = 'gx:EUID',
  LEI_CODE = 'gx:LeiCode',
  VAT_ID = 'gx:VatID',
  TAX_ID = 'gx:TaxID'
}
