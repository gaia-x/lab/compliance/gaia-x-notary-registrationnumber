import { ConfigService } from '@nestjs/config'
import { KeyObject } from 'crypto'
import { KeyLike } from 'jose'

import { KeyBuilder } from '../../utils/key-builder'
import { ConfigServiceMock } from '../test/config-service.mock'
import { PrivateKeyService } from './private-key.service'

describe('PrivateKeyService', () => {
  describe('constructor', () => {
    it.each([null, undefined, '', ' '])('should throw an error when private key is missing "%s"', (privateKey: string) => {
      expect(
        () =>
          new PrivateKeyService(
            new ConfigServiceMock({
              PRIVATE_KEY: null
            }) as unknown as ConfigService
          )
      ).toThrow('Please provide a signing private key through the PRIVATE_KEY environment variable')
    })
  })

  describe('getPrivateKey', () => {
    it('should return a private key', async () => {
      const keyPair = KeyBuilder.buildKeyPair()
      const configService: ConfigService = new ConfigServiceMock({
        PRIVATE_KEY: KeyBuilder.convertToPKCS8(keyPair.privateKey)
      }) as unknown as ConfigService
      const privateKeyService: PrivateKeyService = new PrivateKeyService(configService)

      const privateKey: KeyLike = await privateKeyService.getPrivateKey()

      expect(privateKey).toBeDefined()
      expect(privateKey).toBeInstanceOf(KeyObject)
      expect(privateKey.type).toEqual('private')
    })
  })

  describe('getPrivateKeyAlgorithm', () => {
    it('should return private key algorithm from environment variables', async () => {
      const keyPair = KeyBuilder.buildKeyPair()
      const configService: ConfigService = new ConfigServiceMock({
        PRIVATE_KEY: KeyBuilder.convertToPKCS8(keyPair.privateKey),
        PRIVATE_KEY_ALGORITHM: 'my-alg'
      }) as unknown as ConfigService
      const privateKeyService: PrivateKeyService = new PrivateKeyService(configService)

      expect(privateKeyService.getPrivateKeyAlgorithm()).toEqual('my-alg')
    })
  })
})
