import { ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'
import { DateTime, Duration } from 'luxon'

import { CREDENTIAL_LIFETIME_IN_DAYS } from '../constants'
import { ConfigServiceMock } from '../test/config-service.mock'
import { CertificateService } from './certificate.service'

describe('CertificateService with valid certificate', () => {
  let certificateService: CertificateService
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({ providers: [CertificateService, ConfigService] })
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock({}).withKeyPairAndCertificate())
      .compile()

    certificateService = moduleFixture.get<CertificateService>(CertificateService)
    certificateService.onModuleInit()
  })

  it('should return false if the certificate is not expired', async () => {
    expect(await certificateService.isCertificateExpired()).toBeFalsy()
  })

  it("should return the certificate validTo date if it's lifespan is shorter than now + threshold days", async () => {
    const endDate: DateTime = await certificateService.computeCredentialExpirationDate(600)
    expect(endDate.diff(DateTime.now().plus({ year: 1 })).milliseconds).toBeGreaterThan(-5000)
  })

  it('should return now + threshold if the certificate lifespan is bigger than it', async () => {
    const endDate: DateTime = await certificateService.computeCredentialExpirationDate(CREDENTIAL_LIFETIME_IN_DAYS)
    expect(endDate.diffNow('days').days).toBeCloseTo(CREDENTIAL_LIFETIME_IN_DAYS)
  })

  it('should return a positive amount if expiration date is in the future', async () => {
    expect(await certificateService.getDaysBeforeCertExpirationDate()).toEqual(Duration.fromObject({ year: 1 }).as('days'))
  })
})

describe('CertificateService with expired certificate', () => {
  let certificateService: CertificateService
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({ providers: [CertificateService, ConfigService] })
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock({}).withPrivateKeyAndExpiredCertificate())
      .compile()

    certificateService = moduleFixture.get<CertificateService>(CertificateService)
    certificateService.onModuleInit()
  })

  it('should return true if the certificate is expired', async () => {
    expect(await certificateService.isCertificateExpired()).toBeTruthy()
  })

  it('should return a negative amount if expiration date is in the past', async () => {
    expect(await certificateService.getDaysBeforeCertExpirationDate()).toEqual(-1)
  })
})
