import { Certificate } from '@fidm/x509'
import { Injectable, OnModuleInit } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DateTime } from 'luxon'

import { CREDENTIAL_LIFETIME_IN_DAYS, X509_CERTIFICATE } from '../constants'

@Injectable()
export class CertificateService implements OnModuleInit {
  private pkiCertificate: Certificate = null

  constructor(private readonly configService: ConfigService) {}

  onModuleInit() {
    this.pkiCertificate = Certificate.fromPEM(Buffer.from(this.configService.get<string>(X509_CERTIFICATE)))
  }

  async isCertificateExpired(): Promise<boolean> {
    return DateTime.fromJSDate(this.pkiCertificate.validTo) < DateTime.now()
  }

  /**
   * Computes the expiration date the credential between now + threshold in days, or certificate end date
   * @param threshold number of days the credential needs to be valid for
   */
  async computeCredentialExpirationDate(threshold = CREDENTIAL_LIFETIME_IN_DAYS): Promise<DateTime> {
    const certDate = DateTime.fromJSDate(this.pkiCertificate.validTo)
    const expectedEndDate = DateTime.now().plus({ days: threshold })
    if (certDate < expectedEndDate) {
      return certDate
    } else {
      return expectedEndDate
    }
  }

  async getDaysBeforeCertExpirationDate(): Promise<number> {
    return DateTime.fromJSDate(this.pkiCertificate.validTo).startOf('day').diff(DateTime.now().startOf('day'), 'days').days
  }
}
