import { Certificate } from '@fidm/x509'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { KeyLike, importPKCS8 } from 'jose'

import { PRIVATE_KEY, PRIVATE_KEY_ALGORITHM, X509_CERTIFICATE } from '../../common/constants'
import { KeyBuilder } from '../../utils/key-builder'

@Injectable()
export class PrivateKeyService {
  constructor(private readonly configService: ConfigService) {
    if (!configService.get<string>(PRIVATE_KEY)?.trim()) {
      throw new Error('Please provide a signing private key through the PRIVATE_KEY environment variable')
    }
  }

  async getPrivateKey(): Promise<KeyLike> {
    return await importPKCS8(this.configService.get<string>(PRIVATE_KEY), '')
  }

  getPrivateKeyAlgorithm(): string {
    if (this.configService.get<string>(PRIVATE_KEY_ALGORITHM)?.trim()) {
      return this.configService.get<string>(PRIVATE_KEY_ALGORITHM)
    }
    const certAlgorithmOid = Certificate.fromPEM(Buffer.from(this.configService.get<string>(X509_CERTIFICATE)))
    return KeyBuilder.getPrivateKeyAlgorithm(certAlgorithmOid.signatureOID)
  }
}
