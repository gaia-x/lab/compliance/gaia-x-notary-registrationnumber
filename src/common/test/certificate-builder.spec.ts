import { DateTime } from 'luxon'
import { pki } from 'node-forge'

import { KeyBuilder } from '../../utils/key-builder'
import { CertificateBuilder } from './certificate-builder'

describe('CertificateBuilder', () => {
  it('should generate a self signed certificate', () => {
    const keyPair: pki.KeyPair = KeyBuilder.buildKeyPair()
    const certificate: pki.Certificate = CertificateBuilder.createCertificate(keyPair)

    expect(certificate.publicKey).toEqual(keyPair.publicKey)
    expect(
      DateTime.fromJSDate(certificate.validity.notAfter).diff(DateTime.fromJSDate(certificate.validity.notBefore), 'years').years
    ).toBeGreaterThanOrEqual(1)
    ;(keyPair.publicKey as pki.rsa.PublicKey).verify(null, certificate.signature)
  })
})
