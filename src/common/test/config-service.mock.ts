import { DateTime } from 'luxon'
import { pki } from 'node-forge'

import { KeyBuilder } from '../../utils/key-builder'
import { PRIVATE_KEY, PRIVATE_KEY_ALGORITHM, PS256, PUBLIC_KEY, X509_CERTIFICATE } from '../constants'
import { CertificateBuilder } from './certificate-builder'

/**
 * A simple {@link ConfigService} mock class with a straightforward usage.
 * <p>
 *   For example :
 *   ```js
 *   const configServiceMock = new ConfigServiceMock({
 *     MY_PROPERTY: 'testValue',
 *     ANOTHER_PROPERTY: 'anotherValue',
 *   }) as unknown as ConfigService
 *   ```
 * </p>
 */
export class ConfigServiceMock {
  private readonly properties: Map<string, string>

  constructor(properties: any) {
    this.properties = new Map(Object.entries(properties))
  }

  /**
   * Sets the necessary properties to provide a key pair and a valid certificate.
   */
  withKeyPairAndCertificate(): ConfigServiceMock {
    const keyPair = KeyBuilder.buildKeyPair()

    this.properties.set(PRIVATE_KEY, KeyBuilder.convertToPKCS8(keyPair.privateKey))
    this.properties.set(PUBLIC_KEY, pki.publicKeyToPem(keyPair.publicKey))
    this.properties.set(PRIVATE_KEY_ALGORITHM, PS256)
    this.properties.set(X509_CERTIFICATE, pki.certificateToPem(CertificateBuilder.createCertificate(keyPair)))

    return this
  }

  /**
   * Sets the necessary properties to provide a private key and an expired certificate.
   */
  withPrivateKeyAndExpiredCertificate(): ConfigServiceMock {
    const keyPair = KeyBuilder.buildKeyPair()
    const certificate = CertificateBuilder.createCertificate(keyPair, DateTime.now().minus({ days: 30 }), DateTime.now().minus({ days: 1 }))

    this.properties.set(PRIVATE_KEY, KeyBuilder.convertToPKCS8(keyPair.privateKey))
    this.properties.set(PRIVATE_KEY_ALGORITHM, PS256)
    this.properties.set(X509_CERTIFICATE, pki.certificateToPem(certificate))

    return this
  }

  get(property: string, fallback?: string): string {
    return this.properties.get(property) ?? fallback
  }
}
