import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from '../../common/dto/credential-subject.dto'

export class EoriCredentialSubjectDto extends CredentialSubjectDto {
  @ApiProperty()
  'gx:eori': string

  @ApiProperty()
  'gx:country': string
}
