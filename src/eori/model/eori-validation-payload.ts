import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { ValidationPayload } from '../../common/validator/model/validation-payload'

export class EoriValidationPayload extends ValidationPayload {
  constructor(
    value: string,
    valid: boolean,
    private readonly _country: string,
    evidence: ValidationEvidence,
    invalidationReason?: string
  ) {
    super(RegistrationNumberTypeEnum.EORI, value, valid, evidence, invalidationReason)
  }

  get country(): string {
    return this._country
  }
}
