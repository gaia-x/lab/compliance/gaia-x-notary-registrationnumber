import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { JwtSignatureService } from '../jwt/service/jwt-signature.service'
import { OIDC4VCIModule } from '../oidc4vci/oidc4vci.module'
import { EoriController } from './controller/eori.controller'
import { EoriValidator } from './validator/eori.validator'

@Module({
  imports: [CommonModule, ConfigModule, HttpModule, OIDC4VCIModule],
  controllers: [EoriController],
  providers: [EoriValidator, JwtSignatureService]
})
export class EoriModule {}
