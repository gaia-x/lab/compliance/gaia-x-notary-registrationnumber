import { PreAuthorizedCodeUtils } from '@gaia-x/oidc4vc'
import { Controller, Get, Header, HttpCode, HttpException, HttpStatus, Inject, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiExcludeEndpoint,
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProduces,
  ApiQuery,
  ApiTags
} from '@nestjs/swagger'
import { lastValueFrom } from 'rxjs'

import { DocumentDto, VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { CertificateExpiryGuard } from '../../common/guard/certificate-expiry.guard'
import { RegistrationNumberInputGuard } from '../../common/guard/registration-number-input.guard'
import { DocumentMapper } from '../../common/mapper/document.mapper'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { OIDC4VCIService } from '../../oidc4vci/oidc4vci.service'
import { EoriCredentialSubjectDto } from '../dto/eori-credential-subject.dto'
import { EoriDocumentMapper } from '../mapper/eori-document.mapper'
import { EoriValidationPayload } from '../model/eori-validation-payload'
import { EoriValidator } from '../validator/eori.validator'

@ApiTags('registration-number')
@Controller()
@UseGuards(CertificateExpiryGuard, RegistrationNumberInputGuard)
export class EoriController {
  private readonly documentMapper: DocumentMapper<EoriValidationPayload, EoriCredentialSubjectDto>

  constructor(
    @Inject('DidWeb') private readonly didWeb: string,
    @Inject('OntologyVersion') private readonly ontologyVersion: string,
    private readonly eoriValidator: EoriValidator,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly oidc4vciService: OIDC4VCIService
  ) {
    this.documentMapper = new EoriDocumentMapper(this.didWeb, ontologyVersion)
  }

  @Get('registration-numbers/eori/:eori')
  @ApiOperation({
    summary: `Produce a registration number verifiable credential from a Economic Operators Registration and 
    Identification (EORI)`,
    description: `This endpoint checks the given Economic Operators Registration and Identification (EORI)  against the 
    European Taxation Customs API and responds with a verifiable credential attesting the EORI's validity as a 
    registration number`
  })
  @HttpCode(200)
  @Header('Content-Type', 'application/vc+jwt')
  @ApiProduces('Content-Type', 'application/vc+jwt')
  @ApiQuery({
    name: 'vcId',
    description: `The output verifiable credential ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/credentials/123'
  })
  @ApiQuery({
    name: 'subjectId',
    description: `The output credential subject ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/subjects/123'
  })
  @ApiParam({
    name: 'eori',
    description: `The Economic Operators Registration and Identification (EORI) to verify and produce the verifiable 
    credential from`,
    type: 'string',
    required: true,
    example: 'FR53740792600014'
  })
  @ApiExtraModels(VerifiableCredentialDto)
  @ApiOkResponse({
    description: `Certified registration number verifiable credential`,
    content: {
      'application/vc+jwt': {
        schema: {
          type: 'string',
          description: 'JSON Web Token'
        },
        example:
          'eyJhbGciOiJQUzI1NiIsImlzcyI6ImRpZDp3ZWI6bG9jYWxob3N0OjMwMDAiLCJraWQiOiJkaWQ6d2ViOmxvY2FsaG9zdDozMDAwI1g1MDktSldLIiwiaWF0IjoxNzIxMDUyNDM5MzIwLCJleHAiOjE3Mjg4Mjg0MzkzMjAsImN0eSI6InZjK2xkIiwidHlwIjoidmMrbGQrand0In0.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9kZXZlbG9wbWVudCMiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvY3JlZGVudGlhbHMvMTIzIiwibmFtZSI6IkVPUkkgUmVnaXN0cmF0aW9uIE51bWJlciIsImRlc2NyaXB0aW9uIjoiRWNvbm9taWMgT3BlcmF0b3JzIFJlZ2lzdHJhdGlvbiBhbmQgSWRlbnRpZmljYXRpb24gcmVnaXN0cmF0aW9uIG51bWJlciIsImlzc3VlciI6ImRpZDp3ZWI6bG9jYWxob3N0OjMwMDAiLCJ2YWxpZEZyb20iOiIyMDI0LTA3LTE1VDE2OjA3OjE5LjMyMCswMjowMCIsInZhbGlkVW50aWwiOiIyMDI0LTEwLTEzVDE2OjA3OjE5LjMyMCswMjowMCIsImNyZWRlbnRpYWxTdWJqZWN0Ijp7ImlkIjoiaHR0cHM6Ly9leGFtcGxlLm9yZy9zdWJqZWN0cy8xMjMiLCJ0eXBlIjoiZ3g6RU9SSSIsImd4OmVvcmkiOiJGUjUzNzQwNzkyNjAwMDE0IiwiZ3g6Y291bnRyeSI6IiJ9LCJldmlkZW5jZSI6eyJneDpldmlkZW5jZU9mIjoiRU9SSSIsImd4OmV2aWRlbmNlVVJMIjoiaHR0cHM6Ly9lYy5ldXJvcGEuZXUvdGF4YXRpb25fY3VzdG9tcy9kZHMyL2Vvcy92YWxpZGF0aW9uL3NlcnZpY2VzL3ZhbGlkYXRpb24iLCJneDpleGVjdXRpb25EYXRlIjoiMjAyNC0wNy0xNVQxNjowNzoxOS4zMjArMDI6MDAifX0.qgJAO9jWBoHA5ywXn4rZVBavH8HgsrTvKdgdZjOAXUnqCVh2kmkM4Ew9amnjqkTW04lrLeTOhG5q2g09RcxC3Dp4VImEkO5DPXhHQP879bPtN2TcAKXGtlWQTiYFh4sp_uELRsRArZYcAcDVkbra-0-L8ECKnPkS0xx1x0agJeC5L7gHRzU1pyrFLEw0NxalT2iFc2hRn4CwehWYQ4C8mqarq9QZF3U94Jwrkg4dCP464Mp5LkwD_QE_fFrw0yNiW_S-bmbDXIW3ZhN6UjyZxulIU6eBRGtm1QbYWGmbyQo2hGnGTgjy0ofcSQ3YJZt5OUVu2r4rwdC9q6hDzrRQEg'
      }
    }
  })
  @ApiBadRequestResponse({
    description: `Invalid legal registration number verifiable credential`,
    content: {
      'application/json': {
        examples: {
          'Payload missing': {
            value: {
              statusCode: 400,
              message: 'The request payload is missing'
            }
          },
          'Verifiable credential ID missing': {
            value: {
              statusCode: 400,
              message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
            }
          },
          'Subject ID missing': {
            value: {
              statusCode: 400,
              message: 'The subject ID is missing, please provide it through the subjectId query parameter'
            }
          },
          'Invalid registration number or API error': {
            value: {
              statusCode: 400,
              message: `The given EORI: [FR53740792600014] couldn't be validated: error message`
            }
          }
        }
      }
    }
  })
  async checkEori(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('eori') eori: string): Promise<string> {
    const validationPayload: EoriValidationPayload = await lastValueFrom(this.eoriValidator.validate(eori))

    if (!validationPayload.valid) {
      throw new HttpException(
        `The given EORI: [${eori}] couldn't be validated: ${validationPayload.invalidationReason}`,
        HttpStatus.BAD_REQUEST
      )
    }

    const document: DocumentDto = this.documentMapper.mapFromValidationPayload(vcId, subjectId, validationPayload)

    return this.jwtSignatureService.signVerifiableCredential(document)
  }

  @ApiExcludeEndpoint()
  @Get('registration-numbers/eori/:eori/credential')
  async issueEori(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('eori') eori: string) {
    const signedDocument = await this.checkEori(vcId, subjectId, eori)
    // @ts-expect-error Will be fixed when @gaia-x/oidc4vc support https://www.w3.org/TR/vc-data-model-2.0/
    return await this.oidc4vciService.createCredentialOffer(PreAuthorizedCodeUtils.generate(), signedDocument)
  }
}
