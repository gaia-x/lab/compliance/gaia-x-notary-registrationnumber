import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import { JWTVerifyResult, importSPKI, jwtVerify } from 'jose'
import * as jsonld from 'jsonld'
import { DateTime } from 'luxon'
import * as request from 'supertest'

import { PRIVATE_KEY_ALGORITHM, PUBLIC_KEY } from '../../common/constants'
import { VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { EoriModule } from '../eori.module'
import { EoriValidatorMock, INVALID_EORI, VALID_EORI } from '../test/eori-validator.mock'
import { EoriValidator } from '../validator/eori.validator'

jest.mock('jsonld')

describe('EoriController', () => {
  let app: NestApplication
  const mockConfigService: ConfigServiceMock = new ConfigServiceMock({
    BASE_URL: 'http://127.0.0.1:3000/main',
    OFFLINE_CONTEXTS: 'false'
  }).withKeyPairAndCertificate()

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [EoriModule]
    })
      .overrideProvider(EoriValidator)
      .useValue(EoriValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app?.close()
  })

  describe('/registration-numbers/eori/:eori', () => {
    it('should validate and sign a verifiable credential', async () => {
      ;(jsonld.canonize as jest.Mock).mockResolvedValue('normalizedVerifiableCredential')

      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/eori/${VALID_EORI}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(200)
        .expect('Content-Type', 'application/vc+jwt; charset=utf-8')

      const publicKey = await importSPKI(mockConfigService.get(PUBLIC_KEY), mockConfigService.get(PRIVATE_KEY_ALGORITHM))
      const results: JWTVerifyResult<VerifiableCredentialDto> = await jwtVerify(response.text, publicKey)
      expect(results).toBeDefined()
      expect(results.payload).toBeDefined()
      expect(results.payload.id).toBeDefined()

      expect(results.payload['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
      expect(results.payload.id).toEqual('https://example.org/credentials/123')
      expect(results.payload.name).toEqual('EORI Registration Number')
      expect(results.payload.description).toEqual('Economic Operators Registration and Identification registration number')
      expect(results.payload.issuer).toEqual('did:web:127.0.0.1:3000:main')
      expect(DateTime.fromISO(results.payload.validFrom).diffNow().milliseconds).toBeLessThan(5000)
      expect(results.payload.validUntil).not.toBeNull()
      // Credential subject assertions
      expect(results.payload.credentialSubject.id).toEqual('https://example.org/subjects/123')
      expect(results.payload.credentialSubject.type).toEqual('gx:EORI')
      expect(results.payload.credentialSubject['gx:eori']).toEqual(VALID_EORI)
      expect(results.payload.credentialSubject['gx:country']).toEqual('France')
      // Evidence assertions
      expect(results.payload.evidence['gx:evidenceOf']).toEqual('gx:EORI')
      expect(results.payload.evidence['gx:evidenceURL']).toEqual('https://validator.eu/checkEoriService')
    })

    it('should return a bad request if the verifiable credential ID is missing', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/eori/${VALID_EORI}`)
        .query({
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
        })
    })

    it('should return a bad request if the subject ID is missing', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/eori/${VALID_EORI}`)
        .query({
          vcId: 'https://example.org/credentials/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: 'The subject ID is missing, please provide it through the subjectId query parameter'
        })
    })

    it('should return a bad request if the EORI is invalid', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/eori/${INVALID_EORI}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: `The given EORI: [${INVALID_EORI}] couldn't be validated: EORI validation error message`
        })
    })
  })
})

describe('EoriController with expired certificate', () => {
  let app: NestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [EoriModule]
    })
      .overrideProvider(EoriValidator)
      .useValue(EoriValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'http://127.0.0.1:3000/main',
          OFFLINE_CONTEXTS: 'false'
        }).withPrivateKeyAndExpiredCertificate()
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app?.close()
  })

  describe('/registration-numbers/eori/:eori', () => {
    it('should throw an expired certificate error', async () => {
      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/eori/${VALID_EORI}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(500)
        .expect('Content-Type', 'application/json; charset=utf-8')

      expect(response.body).toEqual({
        message: 'This instance certificate has expired and it will not be able to emit credentials',
        error: 'Internal Server Error',
        statusCode: 500
      })
    })
  })
})
