import { DocumentMapper } from '../../common/mapper/document.mapper'
import { EoriCredentialSubjectDto } from '../dto/eori-credential-subject.dto'
import { EoriValidationPayload } from '../model/eori-validation-payload'
import { EoriCredentialSubjectMapper } from './eori-credential-subject.mapper'

export class EoriDocumentMapper extends DocumentMapper<EoriValidationPayload, EoriCredentialSubjectDto> {
  constructor(didWeb: string, ontologyVersion: string) {
    super(
      didWeb,
      ontologyVersion,
      new EoriCredentialSubjectMapper(),
      'EORI Registration Number',
      'Economic Operators Registration and Identification registration number'
    )
  }
}
