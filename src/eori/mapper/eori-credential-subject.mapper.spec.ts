import { EoriValidationPayload } from '../model/eori-validation-payload'
import { VALID_EORI } from '../test/eori-validator.mock'
import { EoriCredentialSubjectMapper } from './eori-credential-subject.mapper'

describe('EoriCredentialSubjectMapper', () => {
  it('should map an EORI validation payload to a credential subject', () => {
    const mapper: EoriCredentialSubjectMapper = new EoriCredentialSubjectMapper()
    const validationPayload: EoriValidationPayload = new EoriValidationPayload(VALID_EORI, true, 'France', null)

    expect(mapper.map(validationPayload, 'https://example.org/subjects/123')).toEqual({
      id: 'https://example.org/subjects/123',
      type: 'gx:EORI',
      'gx:eori': VALID_EORI,
      'gx:country': 'France'
    })
  })
})
