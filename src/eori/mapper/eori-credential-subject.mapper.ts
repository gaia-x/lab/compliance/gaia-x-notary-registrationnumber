import { CredentialSubjectMapper } from '../../common/mapper/credential-subject.mapper'
import { EoriCredentialSubjectDto } from '../dto/eori-credential-subject.dto'
import { EoriValidationPayload } from '../model/eori-validation-payload'

export class EoriCredentialSubjectMapper implements CredentialSubjectMapper<EoriValidationPayload, EoriCredentialSubjectDto> {
  map(payload: EoriValidationPayload, subjectId: string): EoriCredentialSubjectDto {
    return {
      id: subjectId,
      type: 'gx:EORI',
      'gx:eori': payload.value,
      'gx:country': payload.country
    }
  }
}
