import { DateTime } from 'luxon'

import { DocumentDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { EoriValidationPayload } from '../model/eori-validation-payload'
import { VALID_EORI } from '../test/eori-validator.mock'
import { EoriDocumentMapper } from './eori-document.mapper'

describe('EoriDocumentMapper', () => {
  it('should map an EORI validation payload to a document', () => {
    const mapper: EoriDocumentMapper = new EoriDocumentMapper('did:web:gaia-x.eu', 'development')

    const document: DocumentDto = mapper.mapFromValidationPayload(
      'https://example.org/credentials/123',
      'https://example.org/subjects/123',
      new EoriValidationPayload(
        VALID_EORI,
        true,
        'France',
        new ValidationEvidence('https://eori.org/check/', DateTime.now(), RegistrationNumberTypeEnum.EORI)
      )
    )

    expect(document.name).toEqual('EORI Registration Number')
    expect(document.description).toEqual('Economic Operators Registration and Identification registration number')
  })
})
