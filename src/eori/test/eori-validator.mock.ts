import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { EoriValidationPayload } from '../model/eori-validation-payload'

export const VALID_EORI: string = 'FR53740792600014'
export const INVALID_EORI: string = 'FR01234567891011'

export const EoriValidatorMock = {
  validate: jest.fn((eori: string): Observable<EoriValidationPayload> => {
    return new Observable(subscriber => {
      const valid = eori === VALID_EORI

      subscriber.next(
        new EoriValidationPayload(
          eori,
          valid,
          valid ? 'France' : null,
          new ValidationEvidence('https://validator.eu/checkEoriService', DateTime.now(), RegistrationNumberTypeEnum.EORI),
          'EORI validation error message'
        )
      )
      subscriber.complete()
    })
  })
}
