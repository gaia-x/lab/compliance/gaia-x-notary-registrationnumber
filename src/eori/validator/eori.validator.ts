import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DateTime } from 'luxon'
import { Observable, catchError, map, of } from 'rxjs'
import { DOMParserImpl as dom } from 'xmldom-ts'
import * as xpath from 'xpath-ts'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { RegistrationNumberValidator } from '../../common/validator/registration-number.validator'
import { EoriValidationPayload } from '../model/eori-validation-payload'

@Injectable()
export class EoriValidator implements RegistrationNumberValidator<EoriValidationPayload> {
  private readonly logger = new Logger(EoriValidator.name)

  private readonly europeanCommissionApi: string

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
  ) {
    this.europeanCommissionApi = this.configService.get<string>('EORI_VALIDATION_API')?.trim()

    if (!this.europeanCommissionApi) {
      throw new Error('Please provide the EORI validation API URL through EORI_VALIDATION_API')
    }

    this.logger.debug(`Initializing EORI validator with (API: ${this.europeanCommissionApi})`)
  }

  /**
   * Validates the given EORI registration number against the European Commission's API.
   *
   * @param eori
   */
  validate(eori: string): Observable<EoriValidationPayload> {
    const soapEnvelope: string = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:eori="http://eori.ws.eos.dds.s/">
<soapenv:Header/>
<soapenv:Body>
   <eori:validateEORI>
      <eori:eori>${eori}</eori:eori>
   </eori:validateEORI>
</soapenv:Body>
</soapenv:Envelope>`

    this.logger.log(`Calling the European Commission API to validate EORI ${eori}`)
    return this.httpService
      .post(this.europeanCommissionApi, soapEnvelope, {
        headers: {
          'Content-Type': 'text/xml'
        }
      })
      .pipe(
        map(response => {
          const eoriResult: Document = new dom().parseFromString(response.data)

          return new EoriValidationPayload(
            xpath.select('string(//eori)', eoriResult) as string,
            (xpath.select('number(//status)', eoriResult) as number) === 0,
            xpath.select('string(//country)', eoriResult) as string,
            new ValidationEvidence(this.europeanCommissionApi, DateTime.now(), RegistrationNumberTypeEnum.EORI)
          )
        }),
        catchError(error => {
          this.logger.warn(`Failed to validate EORI with value ${eori} : ${error}`)

          return of(
            new EoriValidationPayload(
              eori,
              false,
              null,
              new ValidationEvidence(this.europeanCommissionApi, DateTime.now(), RegistrationNumberTypeEnum.EORI)
            )
          )
        })
      )
  }
}
