import { HttpModule, HttpService } from '@nestjs/axios'
import { Logger } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { DateTime } from 'luxon'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { EoriValidationPayload } from '../model/eori-validation-payload'
import { VALID_EORI } from '../test/eori-validator.mock'
import { EoriValidator } from './eori.validator'

describe('EoriValidator constructor', () => {
  const configService: ConfigService = new ConfigService()
  const httpService: HttpService = new HttpService()

  it.each(['', undefined, null, ' '])('should throw an error when EORI_VALIDATION_API is missing', eoriValidationApi => {
    jest.spyOn(configService, 'get').mockImplementation(() => eoriValidationApi)

    expect(() => new EoriValidator(configService, httpService)).toThrow('Please provide the EORI validation API URL through EORI_VALIDATION_API')
  })
})

describe('EoriValidator', () => {
  const europeanCommissionApi = 'https://validator.eu/'

  let axiosMock: MockAdapter
  let eoriValidator: EoriValidator

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [EoriValidator]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          EORI_VALIDATION_API: europeanCommissionApi
        })
      )
      .setLogger(new Logger())
      .compile()

    eoriValidator = moduleRef.get<EoriValidator>(EoriValidator)

    axiosMock = new MockAdapter(axios)
  })

  it('should validate the EORI against the European Commission API', done => {
    axiosMock.onPost(europeanCommissionApi).reply(
      200,
      `<?xml version='1.0' encoding='UTF-8'?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns0:validateEORIResponse xmlns:ns0="http://eori.ws.eos.dds.s/">
            <return>
                <requestDate>${DateTime.now().toFormat('dd/MM/yyyy')}</requestDate>
                <result>
                    <eori>${VALID_EORI}</eori>
                    <status>0</status>
                    <statusDescr>Valid</statusDescr>
                    <name>RUE DU COMMERCE</name>
                    <address>RUE DU COMMERCE</address>
                    <street>118 RUE DE JAVEL</street>
                    <postalCode>75015</postalCode>
                    <city>PARIS 15</city>
                    <country>France</country>
                </result>
            </return>
        </ns0:validateEORIResponse>
    </S:Body>
</S:Envelope>`
    )

    eoriValidator.validate(VALID_EORI).subscribe({
      next(result: EoriValidationPayload) {
        assertResult(result, true, 'France')
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage partial responses from the European Commission API', done => {
    axiosMock.onPost(europeanCommissionApi).reply(
      200,
      `<?xml version='1.0' encoding='UTF-8'?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns0:validateEORIResponse xmlns:ns0="http://eori.ws.eos.dds.s/">
            <return>
                <requestDate>${DateTime.now().toFormat('dd/MM/yyyy')}</requestDate>
                <result>
                    <eori>${VALID_EORI}</eori>
                    <status>0</status>
                    <statusDescr>Valid</statusDescr>
                </result>
            </return>
        </ns0:validateEORIResponse>
    </S:Body>
</S:Envelope>`
    )

    eoriValidator.validate(VALID_EORI).subscribe({
      next(result: EoriValidationPayload) {
        assertResult(result, true, '')
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage invalid EORI responses', done => {
    axiosMock.onPost(europeanCommissionApi).reply(
      200,
      `<?xml version='1.0' encoding='UTF-8'?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns0:validateEORIResponse xmlns:ns0="http://eori.ws.eos.dds.s/">
            <return>
                <requestDate>${DateTime.now().toFormat('dd/MM/yyyy')}</requestDate>
                <result>
                    <eori>FR42279772000122</eori>
                    <status>1</status>
                    <statusDescr>Not valid</statusDescr>
                </result>
            </return>
        </ns0:validateEORIResponse>
    </S:Body>
</S:Envelope>`
    )

    eoriValidator.validate(VALID_EORI).subscribe({
      next(result: EoriValidationPayload) {
        assertResult(result, false, '')
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage error responses', done => {
    axiosMock.onPost(europeanCommissionApi).reply(500)

    eoriValidator.validate(VALID_EORI).subscribe({
      next(result: EoriValidationPayload) {
        assertResult(result, false, null)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  function assertResult(result: EoriValidationPayload, isValid: boolean, country: string) {
    expect(result.type).toEqual(RegistrationNumberTypeEnum.EORI)
    expect(result.valid).toEqual(isValid)
    expect(result.country).toEqual(country)
    expect(result.evidence.url).toEqual(europeanCommissionApi)
    expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
    expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.EORI)
  }
})
