import { pki } from 'node-forge'

/*
 * Mapping of OID and JWS
 * https://www.rfc-editor.org/rfc/rfc7518.html#appendix-A
 * */
const OIDJWSMapping = new Map<string, string>([
  ['1.2.840.113549.2.9', 'HS256'],
  ['1.2.840.113549.2.10', 'HS384'],
  ['1.2.840.113549.2.11', 'HS512'],
  ['1.2.840.113549.1.1.11', 'RS256'],
  ['1.2.840.113549.1.1.12', 'RS384'],
  ['1.2.840.113549.1.1.13', 'RS512'],
  ['1.2.840.10045.4.3.2', 'ES256'],
  ['1.2.840.10045.4.3.3', 'ES384'],
  ['1.2.840.10045.4.3.4', 'ES512'],
  ['1.2.840.113549.1.1.10', 'PS256'],
  ['1.2.840.113549.1.1.10', 'PS384'],
  ['1.2.840.113549.1.1.10', 'PS512']
])

export class KeyBuilder {
  static buildKeyPair(): pki.rsa.KeyPair {
    return pki.rsa.generateKeyPair(2048)
  }

  static convertToPKCS8(privateKey: pki.PrivateKey): string {
    const asn1PrivateKey = pki.privateKeyToAsn1(privateKey)
    const privateKeyInfo = pki.wrapRsaPrivateKey(asn1PrivateKey)

    return pki.privateKeyInfoToPem(privateKeyInfo)
  }

  static getPrivateKeyAlgorithm(oid: string): string {
    return OIDJWSMapping.get(oid)
  }
}
