import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Header, Inject, UseInterceptors } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'

import * as packageJson from '../package.json'

@ApiTags('gx-notary')
@Controller()
@UseInterceptors(CacheInterceptor)
export class AppController {
  private readonly baseUrl: string

  constructor(
    private readonly configService: ConfigService,
    @Inject('OntologyVersion') private readonly ontologyVersion: string
  ) {
    this.baseUrl = this.configService.get<string>('BASE_URL')?.trim()

    if (!this.baseUrl) {
      throw new Error(`Please provide this service's base url through the BASE_URL environment variable`)
    }
  }

  @Get()
  @Header('Content-Type', 'application/json')
  @ApiOkResponse({
    description: `Gaia-X Notary application information`,
    content: {
      'application/json': {
        example: {
          software: 'gaia-x-notary-registration-number',
          description: 'Gaia-X registration number verification API service',
          version: '2.0.0',
          documentation: 'https://registration.lab.gaia-x.eu/development/docs/',
          ontologyVersion: '24.04',
          repository: {
            type: 'git',
            url: 'git@gitlab.com:gaia-x/lab/compliance/gaia-x-notary-registrationnumber.git'
          },
          bugs: {
            url: 'https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/-/issues'
          },
          features: {
            taxIDSupported: true
          }
        }
      }
    }
  })
  public getApplicationInformation() {
    return {
      software: packageJson.name,
      description: packageJson.description,
      version: packageJson.version,
      documentation: `${this.baseUrl}/docs/`,
      ontologyVersion: this.ontologyVersion,
      repository: packageJson.repository.url,
      bugs: packageJson.bugs.url,
      features: {
        taxIDSupported: this.configService.get<string>('OPEN_CORPORATES_VALIDATION_API_KEY', '')?.trim()?.length > 0
      }
    }
  }
}
