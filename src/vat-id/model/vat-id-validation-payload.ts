import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { ValidationPayload } from '../../common/validator/model/validation-payload'

export class VatIdValidationPayload extends ValidationPayload {
  constructor(
    value: string,
    valid: boolean,
    public readonly countryCode: string,
    evidence: ValidationEvidence,
    public readonly invalidationReason?: string
  ) {
    super(RegistrationNumberTypeEnum.VAT_ID, value, valid, evidence, invalidationReason)
  }
}
