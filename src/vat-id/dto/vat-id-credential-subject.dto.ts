import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from '../../common/dto/credential-subject.dto'

export class VatIdCredentialSubjectDto extends CredentialSubjectDto {
  @ApiProperty()
  'gx:vatID': string

  @ApiProperty()
  'gx:countryCode': string
}
