import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DateTime } from 'luxon'
import { Observable, catchError, map, of } from 'rxjs'
import { DOMParserImpl as dom } from 'xmldom-ts'
import * as xpath from 'xpath-ts'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { RegistrationNumberValidator } from '../../common/validator/registration-number.validator'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'

@Injectable()
export class VatIdValidator implements RegistrationNumberValidator<VatIdValidationPayload> {
  private readonly logger = new Logger(VatIdValidator.name)

  private readonly checkVatServiceApi: string

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
  ) {
    this.checkVatServiceApi = this.configService.get<string>('VAT_ID_VALIDATION_API')?.trim()

    if (!this.checkVatServiceApi) {
      throw new Error('Please provide the VAT ID validation service URL through VAT_ID_VALIDATION_API')
    }

    this.logger.debug(`Initializing VAT ID registration number validator with (API : ${this.checkVatServiceApi})`)
  }

  validate(vatId: string): Observable<VatIdValidationPayload> {
    if (!vatId || !vatId.trim()) {
      throw new Error('The VAT ID was not provided')
    }

    const countryCode = vatId.slice(0, 2)
    const vatNumber = vatId.substring(2)

    const soapEnvelope: string = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <tns1:checkVat xmlns:tns1="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
      <tns1:countryCode>${countryCode}</tns1:countryCode>
      <tns1:vatNumber>${vatNumber}</tns1:vatNumber>
    </tns1:checkVat>
  </soap:Body>
</soap:Envelope>`

    this.logger.log(`Validating VAT ID ${vatId} against the European Commission API`)

    return this.httpService
      .post(this.checkVatServiceApi, soapEnvelope, {
        headers: { 'Content-Type': 'text/xml' }
      })
      .pipe(
        map(response => {
          const validationResult: Document = new dom().parseFromString(response.data)

          const select = xpath.useNamespaces({
            ns2: 'urn:ec.europa.eu:taxud:vies:services:checkVat:types',
            env: 'http://schemas.xmlsoap.org/soap/envelope/'
          })

          if (select('boolean(//faultcode)', validationResult)) {
            const errorCode = select('string(//env:Fault/faultstring)', validationResult) as string
            let invalidationReason: string
            if (errorCode.indexOf('MS_MAX_CONCURRENT_REQ') > -1) {
              invalidationReason = `The VAT ID ${vatId} could not be validated due to rate limiting in the VAT API`
            } else if (errorCode.indexOf('INVALID_INPUT') > -1) {
              invalidationReason = `The VAT ID ${vatId} could not be validated because it's malformed`
            } else {
              invalidationReason = `The VAT ID ${vatId} could not be validated due to an unknown error in the VAT API`
            }
            this.logger.warn(`An error occurred while trying to validate VAT ID ${vatId}: ${invalidationReason}`)
            return new VatIdValidationPayload(
              vatId,
              false,
              null,
              new ValidationEvidence(this.checkVatServiceApi, DateTime.now(), RegistrationNumberTypeEnum.VAT_ID),
              invalidationReason
            )
          }

          return new VatIdValidationPayload(
            (select('string(//ns2:countryCode)', validationResult) as string) + select('string(//ns2:vatNumber)', validationResult),
            select('string(//ns2:valid)', validationResult) === 'true',
            select('string(//ns2:countryCode)', validationResult) as string,
            new ValidationEvidence(this.checkVatServiceApi, DateTime.now(), RegistrationNumberTypeEnum.VAT_ID)
          )
        }),
        catchError(error => {
          this.logger.warn(`Failed to validate VAT ID with value ${vatId} : ${error}`)

          return of(
            new VatIdValidationPayload(
              vatId,
              false,
              null,
              new ValidationEvidence(this.checkVatServiceApi, DateTime.now(), RegistrationNumberTypeEnum.VAT_ID),
              'An unknown error occurred while calling the VAT API'
            )
          )
        })
      )
  }
}
