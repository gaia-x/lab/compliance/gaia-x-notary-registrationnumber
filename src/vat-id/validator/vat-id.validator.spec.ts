import { HttpModule, HttpService } from '@nestjs/axios'
import { Logger } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { DateTime } from 'luxon'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'
import { INVALID_VAT_ID, VALID_VAT_ID } from '../test/vat-id-validator.mock'
import { VatIdValidator } from './vat-id.validator'

describe('VatIdValidator constructor', () => {
  it.each(['', undefined, null, ' '])('should throw an error when VAT_ID_VALIDATION_API is missing', vatIdValidationApi => {
    const configService: ConfigService = new ConfigService()
    const httpService: HttpService = new HttpService()

    jest.spyOn(configService, 'get').mockImplementation(() => vatIdValidationApi)

    expect(() => new VatIdValidator(configService, httpService)).toThrow(
      'Please provide the VAT ID validation service URL through VAT_ID_VALIDATION_API'
    )
  })
})

describe('VatIdValidator', () => {
  const checkVatService = 'https://validator.eu/checkVatService'

  let axiosMock: MockAdapter
  let vatIdValidator: VatIdValidator

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [VatIdValidator]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          VAT_ID_VALIDATION_API: checkVatService
        })
      )
      .setLogger(new Logger())
      .compile()

    vatIdValidator = moduleRef.get<VatIdValidator>(VatIdValidator)

    axiosMock = new MockAdapter(axios)
  })

  it('should validate the VAT ID against the Check VAT API', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <ns2:checkVatResponse xmlns:ns2="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
                  <ns2:countryCode>${VALID_VAT_ID.substring(0, 2)}</ns2:countryCode>
                  <ns2:vatNumber>${VALID_VAT_ID.substring(2)}</ns2:vatNumber>
                  <ns2:requestDate>${DateTime.now().toISODate()}</ns2:requestDate>
                  <ns2:valid>true</ns2:valid>
                  <ns2:name>GAIA-X</ns2:name>
                  <ns2:address>BELGIUM</ns2:address>
              </ns2:checkVatResponse>
          </env:Body>
      </env:Envelope>`
    )

    vatIdValidator.validate(VALID_VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        expect(result.valid).toEqual(true)
        expect(result.value).toEqual(VALID_VAT_ID)
        expect(result.countryCode).toEqual('FR')
        expect(result.evidence.url).toEqual(checkVatService)
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage invalid VAT IDs', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <ns2:checkVatResponse xmlns:ns2="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
                  <ns2:countryCode>${INVALID_VAT_ID.substring(0, 2)}</ns2:countryCode>
                  <ns2:vatNumber>${INVALID_VAT_ID.substring(2)}</ns2:vatNumber>
                  <ns2:requestDate>${DateTime.now().toISODate()}</ns2:requestDate>
                  <ns2:valid>false</ns2:valid>
                  <ns2:name>---</ns2:name>
                  <ns2:address>---</ns2:address>
              </ns2:checkVatResponse>
          </env:Body>
      </env:Envelope>`
    )

    vatIdValidator.validate(INVALID_VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        expect(result.valid).toEqual(false)
        expect(result.value).toEqual(INVALID_VAT_ID)
        expect(result.countryCode).toEqual('FR')
        expect(result.evidence.url).toEqual(checkVatService)
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it.each(['', ' ', undefined, null])('should manage empty VAT IDs', emptyVatId => {
    expect(() => vatIdValidator.validate(emptyVatId)).toThrow('The VAT ID was not provided')
  })

  it('should manage malformed VAT IDs', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <env:Fault>
                  <faultcode>env:Server</faultcode>
                  <faultstring>INVALID_INPUT</faultstring>
              </env:Fault>
          </env:Body>
      </env:Envelope>`
    )

    vatIdValidator.validate(INVALID_VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        assertErrorResponse(result, INVALID_VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should return a proper message on rate-limiting', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <env:Fault>
                <faultcode>env:Server</faultcode>
                <faultstring>MS_MAX_CONCURRENT_REQ</faultstring>
              </env:Fault>
          </env:Body>
      </env:Envelope>`
    )

    vatIdValidator.validate(VALID_VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.invalidationReason).toEqual(`The VAT ID ${VALID_VAT_ID} could not be validated due to rate limiting in the VAT API`)
        done()
      },
      error() {}
    })
  }, 2000)

  it('should return a proper message on unknown error', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <env:Fault>
                <faultcode>env:Server</faultcode>
                <faultstring>UKNOWN_ERROR</faultstring>
              </env:Fault>
          </env:Body>
      </env:Envelope>`
    )

    const validVAT = VALID_VAT_ID
    vatIdValidator.validate(VALID_VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.invalidationReason).toEqual(`The VAT ID ${VALID_VAT_ID} could not be validated due to an unknown error in the VAT API`)
        done()
      },
      error() {}
    })
  }, 2000)

  it('should manage server errors', done => {
    axiosMock.onPost(checkVatService).reply(500)

    vatIdValidator.validate(VALID_VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        assertErrorResponse(result, VALID_VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  function assertErrorResponse(result: VatIdValidationPayload, invalidVatId: string) {
    expect(result.type).toEqual(RegistrationNumberTypeEnum.VAT_ID)
    expect(result.valid).toEqual(false)
    expect(result.value).toEqual(invalidVatId)
    expect(result.countryCode).toBeNull()
    expect(result.evidence.url).toEqual(checkVatService)
    expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
    expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.VAT_ID)
    expect(result.invalidationReason).toBeDefined()
  }
})
