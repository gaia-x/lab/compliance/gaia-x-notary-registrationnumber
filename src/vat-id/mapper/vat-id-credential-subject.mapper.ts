import { CredentialSubjectMapper } from '../../common/mapper/credential-subject.mapper'
import { VatIdCredentialSubjectDto } from '../dto/vat-id-credential-subject.dto'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'

export class VatIdCredentialSubjectMapper implements CredentialSubjectMapper<VatIdValidationPayload, VatIdCredentialSubjectDto> {
  map(payload: VatIdValidationPayload, subjectId: string): VatIdCredentialSubjectDto {
    return {
      id: subjectId,
      type: 'gx:VatID',
      'gx:vatID': payload.value,
      'gx:countryCode': payload.countryCode
    }
  }
}
