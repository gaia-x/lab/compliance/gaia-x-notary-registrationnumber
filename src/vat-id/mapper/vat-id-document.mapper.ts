import { DocumentMapper } from '../../common/mapper/document.mapper'
import { VatIdCredentialSubjectDto } from '../dto/vat-id-credential-subject.dto'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'
import { VatIdCredentialSubjectMapper } from './vat-id-credential-subject.mapper'

export class VatIdDocumentMapper extends DocumentMapper<VatIdValidationPayload, VatIdCredentialSubjectDto> {
  constructor(didWeb: string, ontologyVersion: string) {
    super(didWeb, ontologyVersion, new VatIdCredentialSubjectMapper(), 'VAT ID', 'Value Added Tax Identifier')
  }
}
