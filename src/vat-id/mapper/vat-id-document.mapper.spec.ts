import { DateTime } from 'luxon'

import { DocumentDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'
import { VALID_VAT_ID } from '../test/vat-id-validator.mock'
import { VatIdDocumentMapper } from './vat-id-document.mapper'

describe('EoriDocumentMapper', () => {
  it('should map a VAT ID validation payload to a document', () => {
    const mapper: VatIdDocumentMapper = new VatIdDocumentMapper('did:web:gaia-x.eu', 'development')

    const document: DocumentDto = mapper.mapFromValidationPayload(
      'https://example.org/credentials/123',
      'https://example.org/subjects/123',
      new VatIdValidationPayload(
        VALID_VAT_ID,
        true,
        'FR',
        new ValidationEvidence('https://vat-id.org/check/', DateTime.now(), RegistrationNumberTypeEnum.VAT_ID)
      )
    )

    expect(document['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
    expect(document.name).toEqual('VAT ID')
    expect(document.description).toEqual('Value Added Tax Identifier')
  })
})
