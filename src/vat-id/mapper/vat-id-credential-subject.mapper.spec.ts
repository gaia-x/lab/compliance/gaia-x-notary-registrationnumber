import { VatIdValidationPayload } from '../model/vat-id-validation-payload'
import { VALID_VAT_ID } from '../test/vat-id-validator.mock'
import { VatIdCredentialSubjectMapper } from './vat-id-credential-subject.mapper'

describe('VatIdCredentialSubjectMapper', () => {
  it('should map a VAT ID validation payload to a credential subject', () => {
    const mapper: VatIdCredentialSubjectMapper = new VatIdCredentialSubjectMapper()
    const validationPayload: VatIdValidationPayload = new VatIdValidationPayload(VALID_VAT_ID, true, 'FR', null)

    expect(mapper.map(validationPayload, 'https://example.org/subjects/123')).toEqual({
      id: 'https://example.org/subjects/123',
      type: 'gx:VatID',
      'gx:vatID': VALID_VAT_ID,
      'gx:countryCode': 'FR'
    })
  })
})
