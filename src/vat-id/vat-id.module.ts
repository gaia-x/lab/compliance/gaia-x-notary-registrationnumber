import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CommonModule } from '../common/common.module'
import { JwtSignatureService } from '../jwt/service/jwt-signature.service'
import { OIDC4VCIModule } from '../oidc4vci/oidc4vci.module'
import { VatIdController } from './controller/vat-id.controller'
import { VatIdValidator } from './validator/vat-id.validator'

@Module({
  imports: [CommonModule, ConfigModule, HttpModule, OIDC4VCIModule],
  controllers: [VatIdController],
  providers: [VatIdValidator, JwtSignatureService]
})
export class VatIdModule {}
