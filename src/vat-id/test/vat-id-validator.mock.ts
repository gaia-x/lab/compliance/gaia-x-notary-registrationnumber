import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { ValidationEvidence } from '../../common/validator/model/validation-evidence'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'

export const VALID_VAT_ID: string = 'FR0123456789'
export const INVALID_VAT_ID: string = 'FR9876543210'

export const VatIdValidatorMock = {
  validate: jest.fn((vatId: string): Observable<VatIdValidationPayload> => {
    return new Observable(subscriber => {
      const valid = vatId === VALID_VAT_ID

      subscriber.next(
        new VatIdValidationPayload(
          vatId,
          valid,
          valid ? 'FR' : null,
          new ValidationEvidence('https://validator.eu/checkVatService', DateTime.now(), RegistrationNumberTypeEnum.VAT_ID),
          'VatId validation error message'
        )
      )
      subscriber.complete()
    })
  })
}
