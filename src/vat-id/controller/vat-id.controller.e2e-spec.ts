import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import { JWTVerifyResult, importSPKI, jwtVerify } from 'jose'
import * as jsonld from 'jsonld'
import { DateTime } from 'luxon'
import * as request from 'supertest'

import { PRIVATE_KEY_ALGORITHM, PUBLIC_KEY } from '../../common/constants'
import { VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { INVALID_VAT_ID, VALID_VAT_ID, VatIdValidatorMock } from '../test/vat-id-validator.mock'
import { VatIdValidator } from '../validator/vat-id.validator'
import { VatIdModule } from '../vat-id.module'

jest.mock('jsonld')

describe('VatIdController', () => {
  let app: NestApplication
  const mockConfigService: ConfigServiceMock = new ConfigServiceMock({
    BASE_URL: 'http://127.0.0.1:3000/main',
    OFFLINE_CONTEXTS: 'false'
  }).withKeyPairAndCertificate()

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [VatIdModule]
    })
      .overrideProvider(VatIdValidator)
      .useValue(VatIdValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app?.close()
  })

  describe('/registration-numbers/vat-id/:vatId', () => {
    it('should validate and sign a verifiable credential', async () => {
      ;(jsonld.canonize as jest.Mock).mockResolvedValue('normalizedVerifiableCredential')

      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/vat-id/${VALID_VAT_ID}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(200)
        .expect('Content-Type', 'application/vc+jwt; charset=utf-8')

      const publicKey = await importSPKI(mockConfigService.get(PUBLIC_KEY), mockConfigService.get(PRIVATE_KEY_ALGORITHM))
      const results: JWTVerifyResult<VerifiableCredentialDto> = await jwtVerify(response.text, publicKey)
      expect(results).toBeDefined()
      expect(results.payload).toBeDefined()
      expect(results.payload.id).toBeDefined()

      expect(results.payload['@context']).toEqual(['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'])
      expect(results.payload.id).toEqual('https://example.org/credentials/123')
      expect(results.payload.name).toEqual('VAT ID')
      expect(results.payload.description).toEqual('Value Added Tax Identifier')
      expect(results.payload.issuer).toEqual('did:web:127.0.0.1:3000:main')
      expect(DateTime.fromISO(results.payload.validFrom).diffNow().milliseconds).toBeLessThan(5000)
      expect(results.payload.validUntil).not.toBeNull()
      // Credential subject assertions
      expect(results.payload.credentialSubject.id).toEqual('https://example.org/subjects/123')
      expect(results.payload.credentialSubject.type).toEqual('gx:VatID')
      expect(results.payload.credentialSubject['gx:vatID']).toEqual(VALID_VAT_ID)
      expect(results.payload.credentialSubject['gx:countryCode']).toEqual('FR')
      // Evidence assertions
      expect(results.payload.evidence['gx:evidenceOf']).toEqual('gx:VatID')
      expect(results.payload.evidence['gx:evidenceURL']).toEqual('https://validator.eu/checkVatService')
    })

    it('should return a bad request if the verifiable credential ID is missing', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/vat-id/${VALID_VAT_ID}`)
        .query({
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
        })
    })

    it('should return a bad request if the subject ID is missing', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/vat-id/${VALID_VAT_ID}`)
        .query({
          vcId: 'https://example.org/credentials/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: 'The subject ID is missing, please provide it through the subjectId query parameter'
        })
    })

    it('should return a bad request if the VAT ID is invalid', () => {
      return request(app.getHttpServer())
        .get(`/registration-numbers/vat-id/${INVALID_VAT_ID}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(400, {
          statusCode: 400,
          message: `The given vatID: [${INVALID_VAT_ID}] couldn't be validated: VatId validation error message`
        })
    })
  })
})

describe('VatIdController with expired certificate', () => {
  let app: NestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [VatIdModule]
    })
      .overrideProvider(VatIdValidator)
      .useValue(VatIdValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'http://127.0.0.1:3000/main',
          OFFLINE_CONTEXTS: 'false'
        }).withPrivateKeyAndExpiredCertificate()
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  describe('/registration-numbers/vat-id/:vatId', () => {
    it('should throw an expired certificate error', async () => {
      const response = await request(app.getHttpServer())
        .get(`/registration-numbers/vat-id/${VALID_VAT_ID}`)
        .query({
          vcId: 'https://example.org/credentials/123',
          subjectId: 'https://example.org/subjects/123'
        })
        .send()
        .expect(500)
        .expect('Content-Type', 'application/json; charset=utf-8')

      expect(response.body).toEqual({
        message: 'This instance certificate has expired and it will not be able to emit credentials',
        error: 'Internal Server Error',
        statusCode: 500
      })
    })
  })
})
