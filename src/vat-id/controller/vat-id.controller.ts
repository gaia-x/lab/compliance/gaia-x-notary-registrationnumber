import { PreAuthorizedCodeUtils } from '@gaia-x/oidc4vc'
import { Controller, Get, Header, HttpCode, HttpException, HttpStatus, Inject, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiExcludeEndpoint,
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProduces,
  ApiQuery,
  ApiTags
} from '@nestjs/swagger'
import { lastValueFrom } from 'rxjs'

import { DocumentDto, VerifiableCredentialDto } from '../../common/dto/verifiable-credential.dto'
import { RegistrationNumberTypeEnum } from '../../common/enum/registration-number-type.enum'
import { CertificateExpiryGuard } from '../../common/guard/certificate-expiry.guard'
import { RegistrationNumberInputGuard } from '../../common/guard/registration-number-input.guard'
import { DocumentMapper } from '../../common/mapper/document.mapper'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { OIDC4VCIService } from '../../oidc4vci/oidc4vci.service'
import { VatIdCredentialSubjectDto } from '../dto/vat-id-credential-subject.dto'
import { VatIdDocumentMapper } from '../mapper/vat-id-document.mapper'
import { VatIdValidationPayload } from '../model/vat-id-validation-payload'
import { VatIdValidator } from '../validator/vat-id.validator'

@ApiTags('registration-number')
@Controller()
@UseGuards(CertificateExpiryGuard, RegistrationNumberInputGuard)
export class VatIdController {
  private readonly documentMapper: DocumentMapper<VatIdValidationPayload, VatIdCredentialSubjectDto>

  constructor(
    @Inject('DidWeb') private readonly didWeb: string,
    @Inject('OntologyVersion') private readonly ontologyVersion: string,
    private readonly vatIdValidator: VatIdValidator,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly oidc4vciService: OIDC4VCIService
  ) {
    this.documentMapper = new VatIdDocumentMapper(this.didWeb, ontologyVersion)
  }

  @Get('registration-numbers/vat-id/:vatId')
  @ApiOperation({
    summary: `Produce a registration number verifiable credential from a VAT ID`,
    description: `This endpoint checks the given VAT ID against the european commission's API and responds with a 
    verifiable credential attesting the VAT ID's validity as a registration number`
  })
  @HttpCode(200)
  @Header('Content-Type', 'application/vc+jwt')
  @ApiProduces('Content-Type', 'application/vc+jwt')
  @ApiQuery({
    name: 'vcId',
    description: `The output verifiable credential ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/credentials/123'
  })
  @ApiQuery({
    name: 'subjectId',
    description: `The output credential subject ID`,
    type: 'string',
    required: true,
    example: 'https://example.org/subjects/123'
  })
  @ApiParam({
    name: 'vatId',
    description: `The VAT ID to verify and produce the verifiable credential from`,
    type: 'string',
    required: true,
    example: 'BE0762747721'
  })
  @ApiExtraModels(VerifiableCredentialDto)
  @ApiOkResponse({
    description: `Certified registration number verifiable credential`,
    content: {
      'application/vc+jwt': {
        schema: {
          type: 'string',
          description: 'JSON Web Token'
        },
        example:
          'eyJhbGciOiJQUzI1NiIsImlzcyI6ImRpZDp3ZWI6bG9jYWxob3N0OjMwMDAiLCJraWQiOiJkaWQ6d2ViOmxvY2FsaG9zdDozMDAwI1g1MDktSldLIiwiaWF0IjoxNzIxMDQ4OTQzMzMxLCJleHAiOjAsImN0eSI6InZjK2xkIiwidHlwIjoidmMrbGQrand0In0.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3czaWQub3JnL2dhaWEteC9kZXZlbG9wbWVudCMiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCJdLCJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvY3JlZGVudGlhbHMvMTIzIiwibmFtZSI6IlZBVCBJRCIsImRlc2NyaXB0aW9uIjoiVmFsdWUgQWRkZWQgVGF4IElkZW50aWZpZXIiLCJpc3N1ZXIiOiJkaWQ6d2ViOmxvY2FsaG9zdDozMDAwIiwidmFsaWRGcm9tIjoiMjAyNC0wNy0xNVQxNTowOTowMy4zMzErMDI6MDAiLCJ2YWxpZFVudGlsIjpudWxsLCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6Imh0dHBzOi8vZXhhbXBsZS5vcmcvc3ViamVjdHMvMTIzIiwidHlwZSI6Imd4OlZhdElEIiwiZ3g6dmF0SUQiOiJCRTA3NjI3NDc3MjEiLCJneDpjb3VudHJ5Q29kZSI6IkJFIn0sImV2aWRlbmNlIjp7Imd4OmV2aWRlbmNlT2YiOiJWQVRfSUQiLCJneDpldmlkZW5jZVVSTCI6Imh0dHA6Ly9lYy5ldXJvcGEuZXUvdGF4YXRpb25fY3VzdG9tcy92aWVzL3NlcnZpY2VzL2NoZWNrVmF0U2VydmljZSIsImd4OmV4ZWN1dGlvbkRhdGUiOiIyMDI0LTA3LTE1VDE1OjA5OjAzLjMzMCswMjowMCJ9fQ.fpPFU2xMJgfGMO-SGdu9jFuJCVhU5TVDKDR0VHRA8vf06iJ6reN54Uu_vSOpK_4TqKUCZBzrmO1QY-kBliMj3hAba7c8OFKnVD-WlC7DbQoEUJvw6w2MCoK1rvlUWUR9v-YToDa1Woj5gT1Qw4EJjlKtrXNMD4hldYSYa8auNbp0Ky_5SugfCUqNWvsLiGlMQtYjgwpPKGNVt7Jp2upIj9O6dpxkq65n5dCpbBKfcESyZsedGfZdNBlVtd-xRTk0D8TDIeqQvHPHXUCk9zNTzsXIKX1ictf72cLWM3FZTxr9yWT5yyk9R0FoSZTw2VcoYF8F_z-tATqJC2ScHK2HsA'
      }
    }
  })
  @ApiBadRequestResponse({
    description: `Invalid legal registration number verifiable credential`,
    content: {
      'application/json': {
        examples: {
          'Payload missing': {
            value: {
              statusCode: 400,
              message: 'The request payload is missing'
            }
          },
          'Verifiable credential ID missing': {
            value: {
              statusCode: 400,
              message: 'The verifiable credential ID is missing, please provide it through the vcId query parameter'
            }
          },
          'Subject ID missing': {
            value: {
              statusCode: 400,
              message: 'The subject ID is missing, please provide it through the subjectId query parameter'
            }
          },
          'Invalid registration numbers or API error': {
            value: {
              statusCode: 400,
              message: `The given vatID: [BE0762747721] couldn't be validated: error message`
            }
          }
        }
      }
    }
  })
  async checkVatId(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('vatId') vatId: string): Promise<string> {
    const validationPayload: VatIdValidationPayload = await lastValueFrom(this.vatIdValidator.validate(vatId))

    if (!validationPayload.valid) {
      throw new HttpException(
        `The given vatID: [${vatId}] couldn't be validated: ${validationPayload.invalidationReason}`,
        HttpStatus.BAD_REQUEST
      )
    }

    const document: DocumentDto = this.documentMapper.mapFromValidationPayload(vcId, subjectId, validationPayload)

    return this.jwtSignatureService.signVerifiableCredential(document)
  }

  @ApiExcludeEndpoint()
  @Get('registration-numbers/vat-id/:vatId/credential')
  async issueVatId(@Query('vcId') vcId: string, @Query('subjectId') subjectId: string, @Param('vatId') vatId: string) {
    const signedDocument = await this.checkVatId(vcId, subjectId, vatId)
    // @ts-expect-error Will be fixed when @gaia-x/oidc4vc support https://www.w3.org/TR/vc-data-model-2.0/
    return await this.oidc4vciService.createCredentialOffer(PreAuthorizedCodeUtils.generate(), signedDocument)
  }
}
