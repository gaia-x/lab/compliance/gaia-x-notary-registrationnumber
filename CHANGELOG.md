# [2.9.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.8.1...v2.9.0) (2025-02-20)


### Bug Fixes

* add registration number VC type ([485586f](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/485586f4b4a77bdff5b447e27b30f74afa8f97e5))
* remove node-forge for most of operation ([f184826](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/f18482652be83db5299996ac94a2d4137f3627ce))
* update mime type to application/vc+jwt ([5cc1d81](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/5cc1d811acb4165f2d2f2ecb35294b1f725b471b))


### Features

* **middleware:** add a request/response logging middleware ([bb7215d](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/bb7215d9c57f53a9d956bfaef4ca840ac79c4537))

## [2.8.1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.8.0...v2.8.1) (2024-12-18)


### Bug Fixes

* wrong repository name for tags ([52bdb01](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/52bdb0177726e3c49e24896be0418e806339e1f7))

# [2.8.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.7.0...v2.8.0) (2024-12-18)


### Features

* arm64 build on top of amd64 ([27aa089](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/27aa089f452e5d0b5d3094f48f8923b157135111)), closes [#9](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/issues/9)

# [2.7.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.6.1...v2.7.0) (2024-11-25)


### Bug Fixes

* update several dependencies ([58e4a82](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/58e4a826633085c198f14f676f29afca71d2e7e7))


### Features

* add support for native company number search to Tax ID ([2bd12b3](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/2bd12b391824c4b97aa2fa39812faca909cf0abe))

## [2.6.1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.6.0...v2.6.1) (2024-10-18)


### Bug Fixes

* remove unused imports in test ([0a875d5](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/0a875d59401182dda3ffa8b6ffc2e4dc09a37496))

# [2.6.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.5.0...v2.6.0) (2024-07-30)


### Bug Fixes

* fix merge conflicts, temporarily remove oidc endpoints ([bcf6f9f](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/bcf6f9f92ebbf8a45a282bfd1517d9bf9034f19a))


### Features

* add VC-JWT format as output credential [LAB-691] ([8d3cf93](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/8d3cf932636bf4a1feee916a68c061d77893167c))
* **LAB-663:** migrate to verifiable credential data model v2.0 ([6e92e60](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/6e92e606b9d839b0f6dff388bfff3f462f45f2ae))
* **LAB-664:** update the Gaia-X schema to use the SCWG ontology ([b088770](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/b0887702f3c8ef261aadf758c278d66b22044b1a))
* **LAB-668:** add oidc4vci support ([a7471f9](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/a7471f905c4c1fe605824fa40704bf00f70ced35))
* **LAB-673:** introduce an ontology version environment variable ([df38db6](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/df38db6ebf4bb56ef5a34ede8a3c5c444174c374))

# [2.5.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.4.0...v2.5.0) (2024-06-06)


### Bug Fixes

* add credential expiration date ([90e2454](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/90e24541550209f05dbd7a7162ebbb96156f2029))
* add credential type in vc ([f54167a](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/f54167a18ad221528bf281c5e4fd21abdfc31723))
* add subject type in credential type ([acbeb97](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/acbeb978a380087a1cb0c9890916b3db52b6bcc4))
* do not emit credentials with an outdated cert ([b2ed62a](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/b2ed62a397a4fb0ca00f1af3d192c1e41514ff97))
* hotfix remove schema.org ([4a82a6d](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/4a82a6d3c8ed810d9a58cb32e643fff578ffe807))
* **LAB-632:** document API environment variables ([34ac501](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/34ac501c0217bce789b2a43dab7f3cb127e51162))


### Features

* add a cron to check cert validity date ([a2afbda](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/a2afbdae0bd0b49785cb38fe288b94efe0bccd57))
* **LAB-661:** add schema.org and VCard contexts to offline document loader ([bc9ee4d](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/bc9ee4dbb085b386a99e514d8347f7c4b2be835e))
* **LAB-661:** divide registration number endpoint in multiple endpoints per type ([06f0fe5](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/06f0fe508ecd5631d403422d8ae69dcff295bc7d))

# [2.4.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.3.0...v2.4.0) (2024-04-11)


### Features

* display tax id support in appcontroller ([6afcc67](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/6afcc6742a0ebcd995251e1e717016e471392c6a))

# [2.3.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.2.0...v2.3.0) (2024-04-09)


### Bug Fixes

* provide meaningful error messages ([0273e30](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/0273e304e963a732e6cea5c86e6930d8e7949add))
* **signature:** handle exceptions from the signature library [LAB-538] ([1f83fe6](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/1f83fe6ee81658e91dce79a141d731c037ef1635))


### Features

* implement open corporates validator ([23c84e2](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/23c84e246957eb34bafed62d7dc605cc38cce30c))

# [2.2.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.1.0...v2.2.0) (2024-03-14)


### Features

* **signature:** use json2020signature library and remove redudant code [LAB-477] ([7b0827a](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/7b0827a2fd73114fc1cb139348b5bdd0354dec75))

# [2.1.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v2.0.0...v2.1.0) (2024-01-29)


### Bug Fixes

* **LAB-420:** fix app path management ([28d9ad0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/28d9ad07344321e273570511be875f7f680d28de))


### Features

* **LAB-420:** rewrite the identity module (Did & Cert) ([1d9c33a](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/1d9c33a299e59eb86575ffa334a4027af8893dca))
* **LAB-420:** rewrite the registration number module ([7d54ef0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/7d54ef084c7254d31d6a601e2ab104a5f8c2c4f2))

# [2.0.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.6.2...v2.0.0) (2023-11-21)


### chore

* branch-off tagus loire ([3eb31ca](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/3eb31ca01368561e9e46779ecf4116013411a09c))


### BREAKING CHANGES

* switch to v2

## [1.6.2](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.6.1...v1.6.2) (2023-11-21)


### Bug Fixes

* certificate application type header ([7d7ff04](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/7d7ff042761d22e489231e6fd4a9bf4182cf0584))
* docs link in info page ([621fdc2](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/621fdc20dc9ab0754da8e3436c619a7f59526d34))
* docs url on base page ([5dff135](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/5dff135824d01d7481e4a5f5876841d8ff38fede))
* return error on vat API outage ([471c599](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/471c599db50f918545a7653897d9d42faecd3b2b))

## [1.6.1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.6.0...v1.6.1) (2023-10-17)


### Bug Fixes

* **#3:** distinguish VC subject id from VC id ([f15165b](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/f15165bd279a37a56274f0b4a473adbe1ff289c2)), closes [#3](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/issues/3)

# [1.6.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.5.0...v1.6.0) (2023-10-10)


### Bug Fixes

* **#4:** use local version of context documents ([ac2f526](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/ac2f5269df96dbbe0744fb66b585fb10dbae633e)), closes [#4](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/issues/4)


### Features

* enable cors middleware on info endpoint ([d10395f](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/d10395f401c652446467d50c7466d2066c242a26))

# [1.5.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.4.1...v1.5.0) (2023-08-28)


### Bug Fixes

* **LAB-349:** Make did json W3C compliant ([7c43850](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/7c43850640fffe73dbda09adda61b5ead5acca2c))
* **TAG-181:** Fixup deployment BASE_URL ([f4e0154](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/f4e0154fe966498601b1774de9aff29383b1363c))


### Features

* **LAB-358:** return VC type as an array ([2659f95](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/2659f95486a8595e077b87a4999989d70125af75))

## [1.4.1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.4.0...v1.4.1) (2023-07-26)


### Bug Fixes

* **TAG-130:** credentials issued in the notary ([a91a52f](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/a91a52f2d8e8f0cef8e6d4eb0870701f0190c7df))

# [1.4.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.3.0...v1.4.0) (2023-06-21)


### Features

* publish changelogs on slack ([4360b9e](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/4360b9e859d2338c53f35e9bfe87a52e93c1f416))

# [1.3.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.2.0...v1.3.0) (2023-06-20)


### Features

* add evidence & countryCode from APIs ([aa268d2](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/aa268d221abf294878a5e145744f47d07a4f8798))

# [1.2.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.7...v1.2.0) (2023-06-13)


### Features

* take target VC id as input ([be12f89](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/be12f8952d1f693e18259d17e622860a3426b5f0))

## [1.1.7](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.6...v1.1.7) (2023-05-24)


### Bug Fixes

* add semantic-release gitlab ([74c2253](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/74c225359298cae9f1c4bb93ec56a5124b012090))

## [1.1.6](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.5...v1.1.6) (2023-05-15)


### Bug Fixes

* deploy on registrationnumber.notary sub domain ([77f2a6c](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/77f2a6c78f42cb2e7ba055bac61a5917dd3532f1))

## [1.1.5](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.4...v1.1.5) (2023-04-18)


### Bug Fixes

* provide a docker tagged image ([b5bac01](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/b5bac018ec0e6eb3c26c49b442de4fb4bc14c7d5))
* provide a docker tagged image ([0353056](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/0353056b72d60a02d1963eb71d3ec50eaaf6bbdb))

## [1.1.4](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.3...v1.1.4) (2023-04-18)


### Bug Fixes

* provide a docker tagged image ([4d1cc99](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/4d1cc99a288ffe5cfe88486fcc4608f6d9a939b8))

## [1.1.3](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.2...v1.1.3) (2023-04-11)


### Bug Fixes

* provide a docker tagged image ([03b4a4e](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/03b4a4e68a39766fe10d5c267d694b65e4f7519b))

## [1.1.2](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.1...v1.1.2) (2023-04-11)


### Bug Fixes

* add <major>-latest and <major>-<minor>-latest tags on release ([3f0083a](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/3f0083af092f201a61c18f0e7d96eca33928b49f))
* add <major>-latest and <major>-<minor>-latest tags on release ([cc70ab7](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/cc70ab79f9908aaa9d7dcc5317db65e5e689c06d))
* provide a docker tagged image ([6267f27](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/6267f27ea8733d3a5dafc88e54928c45a584fb68))
* provide a docker tagged image ([f63d8ad](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/f63d8ad11c6e08f55c9f1c75c82124ff377762f2))

## [1.1.1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.1.0...v1.1.1) (2023-04-07)


### Bug Fixes

* add <major>-latest and <major>-<minor>-latest tags on release ([ed234ef](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/ed234efada2e86560a9fe3abf4dc9547605af3ce))

# [1.1.0](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.0.1...v1.1.0) (2023-04-07)


### Features

* refactor eori service add test env ([9c304cf](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/9c304cfd11b7b15444a9fb4905c43c63f71f3568))

## [1.0.1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/compare/v1.0.0...v1.0.1) (2023-04-06)


### Bug Fixes

* deploy on lab on tags ([df272ee](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/df272ee310029273a6c5929bdd3aae4cc5aeb922))

# 1.0.0 (2023-04-06)


### Bug Fixes

* sign service schema url & example values ([15df8a7](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/15df8a71a460a4d184f24aca3e947d3b6b1937b6))
* swagger schema ([83d6156](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/83d6156a8180093881707abeb04b4ee0d0239456))


### Features

* add root controller like on compliance/registry ([4043d1f](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/4043d1fe34dbe8e15f8735067c96bd963e67c0dd))
* optional port var and examples in swagger ([f56fe9c](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/f56fe9cf9f34cd6518e742a44f3dbf442cbe4ff8))
* rename lei to leiCode ([d8e2565](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/d8e2565c2193ae3a1c07ac00b658a5b752f49720))
* udpate registration number format ([9afbbf1](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/commit/9afbbf1838d524aa54c47fc453e39079ed67074c))
